# Version 0.0.9 (2021-01-)

* Added `deny(unsafe_code)` directive.

# Version 0.0.8 (2020-09-09)

* Added a bootstrap method which uses domain names.

# Version 0.0.7 (2020-09-07)

* Now the client can generate deterministic and chan addresses.
* Now the client can send to and receive from chans.
* Redefine database tables for identities.

# Version 0.0.6 (2020-08-25)

* Added a button to copy an address to the clipboard.

# Version 0.0.5 (2020-08-25)

* Now the client can generate random addresses.
* Now the client can send broadcast messages.

# Version 0.0.4 (2020-08-17)

* Now the client receives broadcast messages.

# Version 0.0.3 (2020-07-28)

* Now the node handles onionpeer Bitmessage objects.
* Now the node advertises its own node addresses.

# Version 0.0.2 (2020-06-09)

* Now configurations are saved to the file in the user's configuration directory.
* Now known addresses are sent to the peers when connected.
* Now downloaded objects and list of known node addresses are persistent.

# Version 0.0.1 (2020-05-26)

* Bumped the version due to the update of `koibumi-socks`.

# Version 0.0.0 (2020-05-23)

* Features
    * Connect to the Bitmessage network via Tor SOCKS5 proxy
    * Relay Bitmessage objects
    * Configure several settings on a GUI window
    * Monitor connections and stat of objects on a GUI window

Limitations:

* User messages can not be written or read.
* Known addresses are not advertised.
* All data are not persistent.
