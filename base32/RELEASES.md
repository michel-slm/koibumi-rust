# Version 0.0.2 (2021-01-)

* Added `deny(unsafe_code)` directive.

# Version 0.0.1 (2020-09-07)

* Supports input with arbitrary length.

# Version 0.0 (2020-05-23)

* Features
    * Encode byte array into Base32 string
    * Decode Base32 string into byte array

Limitations:

* Padding is not supported.
* Only lowercase strings are supported.
