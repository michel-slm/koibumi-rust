This crate is a Base58 encoder/decoder library.

The library is intended to be used to implement a [Bitmessage](https://bitmessage.org/) address encoder/decoder.
