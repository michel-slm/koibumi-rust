This crate is an inbox/outbox module for Koibumi (sync version), an experimental Bitmessage client.

See [`koibumi-sync`](https://crates.io/crates/koibumi-sync) for more about the application.
See [Bitmessage](https://bitmessage.org/) for more about the protocol.
