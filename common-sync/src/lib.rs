//! This crate contains common code among GUI and daemon versions for Koibumi (sync version), an experimental Bitmessage client.
//!
//! See [`koibumi-sync`](https://crates.io/crates/koibumi-sync) for more about the application.
//! See [Bitmessage](https://bitmessage.org/) for more about the protocol.

#![deny(unsafe_code)]
#![warn(missing_docs)]

pub mod boxes;
pub mod config;
pub mod constant;
pub mod log;
pub mod node;
pub mod param;
