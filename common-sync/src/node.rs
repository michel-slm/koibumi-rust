//! Functions to prepare database connection for node.

use std::{fmt, io, path::PathBuf};

use log::debug;

use crate::{config::create_data_dir, param::Params};

/// An error which can be returned when operating on the node database.
#[derive(Debug)]
pub enum Error {
    /// A standard I/O error was caught during operation on the node database.
    /// The actual error caught is returned as a payload of this variant.
    IoError(io::Error),
    /// A Rusqlite error was caught during operation on the node database.
    /// The actual error caught is returned as a payload of this variant.
    RusqliteError(rusqlite::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::IoError(err) => err.fmt(f),
            Self::RusqliteError(err) => err.fmt(f),
        }
    }
}

impl std::error::Error for Error {}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}

impl From<rusqlite::Error> for Error {
    fn from(err: rusqlite::Error) -> Self {
        Self::RusqliteError(err)
    }
}

/// Returns a database path.
pub fn prepare(params: &Params) -> Result<PathBuf, Error> {
    let mut path = create_data_dir(params)?;
    path.push("node.db");
    debug!("db: {:?}", path);
    Ok(path)
}
