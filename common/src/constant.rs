//! Declares several constants used by the application.

/// The string representation of the socket address
/// of the Bitmessage seed node for the Tor network.
pub const ONION_SEED: &str = "quzwelsuziwqgpt2.onion:8444";

/// The string representation of bootstrap socket addresses.
pub const BOOTSTRAPS: &[&str] = &[
    "bootstrap8080.bitmessage.org:8080",
    "bootstrap8444.bitmessage.org:8444",
];

/// The string representation of the socket address
/// of the default local Bitmessage server.
pub const LOCAL_SERVER: &str = "127.0.0.1:8444";

/// The string representation of the socket address
/// of the default local Tor SOCKS server.
pub const TOR_SOCKS: &str = "127.0.0.1:9050";
