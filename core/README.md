This crate is the core library for Koibumi, an experimental Bitmessage client.

The library provides various types and methods usable to implement Bitmessage clients.

See [`koibumi`](https://crates.io/crates/koibumi) or [`koibumi-sync`](https://crates.io/crates/koibumi-sync) for more about the application.
See [Bitmessage](https://bitmessage.org/) for more about the protocol.
