# Version 0.0.8 (2021-01-)

* Added `deny(unsafe_code)` directive.

# Version 0.0.7 (2020-09-09)

* Added a converter from a SOCKS socket address to an extended socket address.

# Version 0.0.6 (2020-09-07)

* Added the msg object.
* Redefine identities.

# Version 0.0.5 (2020-08-25)

* Added the methods for generating addresses.
* Added the methods for encrypting messages.

# Version 0.0.4 (2020-08-17)

* Added the broadcast object.
* Added the methods for decrypting the encrypted messages.

# Version 0.0.3 (2020-07-28)

* Added the onionpeer Bitmessage object.
* Added a PoW performing function.
* Redesigned the interfaces of the PoW functions.

# Version 0.0.2 (2020-06-09)

* Now the configuration object can be serialized/deserialized by serde.

# Version 0.0.1 (2020-05-26)

* Bumped the version due to the update of `koibumi-socks`.

# Version 0.0.0 (2020-05-23)

* Types for Bitmessage relay
    * Bitmessage packet and header types
    * Bitmessage message types
    * Bitmessage object header type
    * Proof of work validation function
    * Bitmessage network address types
    * Variable length types: var_int, var_str, var_int_list
    * Time types used by the Bitmessage protocol
    * Hash wrapper functions
    * A configuration object type
    * Serializing/deserializing traits

Limitations:

* Internals of Bitmessage objects are not recognized.
* User identities can not be treated.
* User messages can not be treated.
