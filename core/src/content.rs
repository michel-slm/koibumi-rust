//! Provides Bitmessage message content types.

mod msg;
pub use crate::content::msg::Msg;

mod broadcast;
pub use crate::content::broadcast::Broadcast;
