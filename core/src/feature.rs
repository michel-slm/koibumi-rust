use std::io::{self, Read, Write};

use crate::io::{ReadFrom, WriteTo};

bitflags! {
    /// Public key bitfield features.
    pub struct Features: u32 {
        /// Node can be used to onion-route messages.
        const ONION_ROUTER = 1<<27;
        /// Receiving node supports a forward secrecy encryption extension.
        const FORWARD_SECRECY = 1<<28;
        /// Receiving node supports extended encoding.
        const EXTENDED_ENCODING = 1<<29;
        /// Supports simple recipient verification.
        const INCLUDE_DESTINATION = 1<<30;
        /// If true, the receiving node does send acknowledgements (rather than dropping them).
        const DOES_ACK = 1<<31;
    }
}

impl Features {
    /// Creates features from bits directly.
    pub fn from_bits_direct(bits: u32) -> Self {
        Self { bits }
    }
}

impl WriteTo for Features {
    fn write_to(&self, w: &mut dyn Write) -> io::Result<()> {
        self.bits().write_to(w)
    }
}

impl ReadFrom for Features {
    fn read_from(r: &mut dyn Read) -> io::Result<Self>
    where
        Self: Sized,
    {
        let bits = u32::read_from(r)?;
        Ok(Self::from_bits_direct(bits))
    }
}
