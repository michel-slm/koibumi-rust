use std::io::{self, Read, Write};

use crate::{
    io::{ReadFrom, WriteTo},
    message::Message,
    packet::Command,
};

/// A "verack" message that is sent as a reply to a version message,
/// which indicates the connection is accepted.
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub struct Verack {}

impl Default for Verack {
    fn default() -> Self {
        Self {}
    }
}

impl Verack {
    /// Constructs a verack message.
    pub fn new() -> Self {
        Self::default()
    }
}

impl WriteTo for Verack {
    fn write_to(&self, _w: &mut dyn Write) -> io::Result<()> {
        Ok(())
    }
}

impl ReadFrom for Verack {
    fn read_from(_r: &mut dyn Read) -> io::Result<Self>
    where
        Self: Sized,
    {
        Ok(Self {})
    }
}

impl Message for Verack {
    const COMMAND: Command = Command::VERACK;
}

#[test]
fn test_verack_write_to() {
    let test = Verack::new();
    let mut bytes = Vec::new();
    test.write_to(&mut bytes).unwrap();
    let expected = [];
    assert_eq!(bytes, expected.to_vec());
}

#[test]
fn test_verack_read_from() {
    use std::io::Cursor;

    let mut bytes = Cursor::new([]);
    let test = Verack::read_from(&mut bytes).unwrap();
    let expected = Verack::new();
    assert_eq!(test, expected);
}
