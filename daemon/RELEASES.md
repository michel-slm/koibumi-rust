# Version 0.0.8 (2021-01-)

* Added `deny(unsafe_code)` directive.

# Version 0.0.7 (2020-09-09)

* Updated libraries.

# Version 0.0.6 (2020-09-07)

* Now the dependency on `ctrlc` crate is optional.
* Handles msg objects.

# Version 0.0.5 (2020-08-25)

* Update libraries.

# Version 0.0.4 (2020-08-17)

* Now the client receives broadcast messages.

# Version 0.0.3 (2020-07-28)

* Now the node handles onionpeer Bitmessage objects.
* Now the node advertises its own node addresses.
* Added Ctrl-C signal handler.

# Version 0.0.2 (2020-06-09)

* Now configurations are loaded from the file in the user's configuration directory.
* Now known addresses are sent to the peers when connected.
* Now downloaded objects and list of known node addresses are persistent.

# Version 0.0.1 (2020-05-26)

* Bumped the version due to the update of `koibumi-socks`.

# Version 0.0.0 (2020-05-23)

* Features
    * Connect to the Bitmessage network via Tor SOCKS5 proxy
    * Relay Bitmessage objects

Limitations:

* No configuration is supported.
* User messages can not be written or read.
