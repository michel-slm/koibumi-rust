# Version 0.0.0 (2021-01-)

* Renamed from [`koibumi-socks-core`](https://crates.io/crates/koibumi-socks-core).

----

(Release notes for `koibumi-socks-core`, which is discontinued)

# Version 0.0.1 (2020-09-09)

* Added a parser for the socket address.

# Version 0.0.0 (2020-09-07)

* Split from koibumi-socks.
