use std::{fmt, num::ParseIntError, str::FromStr};

/// A port number of a network socket address.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Port(u16);

impl Port {
    /// Constructs a new port number from its `u16` value.
    pub const fn new(value: u16) -> Self {
        Self(value)
    }

    /// Returns the internal value as `u16`.
    pub fn as_u16(self) -> u16 {
        self.0
    }
}

impl fmt::Display for Port {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl From<u16> for Port {
    fn from(value: u16) -> Self {
        Self(value)
    }
}

impl FromStr for Port {
    type Err = ParseIntError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        Ok(Self::new(s.parse::<u16>()?))
    }
}

#[test]
fn test_port_as_u16() {
    let test = Port::new(1234).as_u16();
    let expected = 1234_u16;
    assert_eq!(test, expected);
}

#[test]
fn test_port_to_string() {
    let test = Port::new(1234).to_string();
    let expected = "1234".to_string();
    assert_eq!(test, expected);
}

#[test]
fn test_port_parse() {
    let test = "1234".parse::<Port>().unwrap();
    let expected = Port::new(1234);
    assert_eq!(test, expected);
}
