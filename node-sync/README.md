This crate is a Bitmessage node implementation as a library for Koibumi (sync version), an experimental Bitmessage client.

See [`koibumi-sync`](https://crates.io/crates/koibumi-sync) for more about the application.
See [Bitmessage](https://bitmessage.org/) for more about the protocol.
