// Based on: Async programming in Rust with async-std
// https://book.async.rs/tutorial/handling_disconnection.html

use std::{
    collections::{hash_map::Entry, HashMap, HashSet},
    convert::TryInto,
    fmt,
    io::{BufReader, BufWriter, Cursor, Read, Write},
    net::{Shutdown, SocketAddr as StdSocketAddr, TcpListener, TcpStream, ToSocketAddrs},
    path::PathBuf,
    sync::{
        atomic::{AtomicBool, AtomicUsize, Ordering},
        Arc,
    },
    thread,
    time::Duration as StdDuration,
};

use crossbeam_channel::{bounded, select, Receiver, Sender};
use log::{debug, error, info, log, trace, Level};
use rand::seq::SliceRandom;

use koibumi_core::{
    address::Address,
    identity::Private as PrivateIdentity,
    io::WriteTo,
    message::{self, InvHash, NodeNonce, Pack, StreamNumber},
    net::SocketAddrExt,
    object,
    packet::{Header, Packet},
    time::Time,
};
use koibumi_socks_sync as socks;

use crate::{
    config::Config,
    connection::{Connection, Direction},
    constant::REQUEST_EXPIRES,
    inv_manager::{manage as manage_invs, Event as InvEvent},
    manager::Event as BmEvent,
    message_handler::dispatch_message,
    net::SocketAddrNode,
    node_manager::{manage as manage_nodes, Entry as NodeEntry, Event as NodeEvent},
    pow_manager::{manage as manage_pows, Event as PowEvent},
    user_manager::{manage as manage_users, Event as UserEvent, User},
};

#[derive(Debug)]
pub struct Context {
    config: Config,
    node_nonce: NodeNonce,

    incoming_initiated: AtomicUsize,
    incoming_connected: AtomicUsize,
    incoming_established: AtomicUsize,
    outgoing_initiated: AtomicUsize,
    outgoing_connected: AtomicUsize,
    outgoing_established: AtomicUsize,

    aborted: Arc<AtomicBool>,

    db_path: PathBuf,

    broker_sender: Sender<Event>,
    bm_event_sender: Sender<BmEvent>,
    node_sender: Sender<NodeEvent>,
    inv_sender: Sender<InvEvent>,
    pow_sender: Sender<PowEvent>,
    user_sender: Sender<UserEvent>,
}

impl Context {
    pub fn config(&self) -> &Config {
        &self.config
    }

    pub fn node_nonce(&self) -> NodeNonce {
        self.node_nonce
    }

    pub fn initiated(&self, direction: Direction) -> &AtomicUsize {
        match direction {
            Direction::Incoming => &self.incoming_initiated,
            Direction::Outgoing => &self.outgoing_initiated,
        }
    }

    pub fn connected(&self, direction: Direction) -> &AtomicUsize {
        match direction {
            Direction::Incoming => &self.incoming_connected,
            Direction::Outgoing => &self.outgoing_connected,
        }
    }

    pub fn established(&self, direction: Direction) -> &AtomicUsize {
        match direction {
            Direction::Incoming => &self.incoming_established,
            Direction::Outgoing => &self.outgoing_established,
        }
    }

    pub fn aborted(&self) -> &Arc<AtomicBool> {
        &self.aborted
    }

    pub fn abort(&self) {
        self.aborted.store(true, Ordering::SeqCst);
    }

    pub fn db_path(&self) -> &PathBuf {
        &self.db_path
    }

    pub fn broker_sender(&self) -> &Sender<Event> {
        &self.broker_sender
    }

    pub fn bm_event_sender(&self) -> &Sender<BmEvent> {
        &self.bm_event_sender
    }

    pub fn node_sender(&self) -> &Sender<NodeEvent> {
        &self.node_sender
    }

    pub fn inv_sender(&self) -> &Sender<InvEvent> {
        &self.inv_sender
    }

    pub fn pow_sender(&self) -> &Sender<PowEvent> {
        &self.pow_sender
    }

    pub fn user_sender(&self) -> &Sender<UserEvent> {
        &self.user_sender
    }
}

#[derive(Debug)]
pub struct ConnectionManager {
    ctx: Arc<Context>,

    broker_shutdown_sender: Sender<ShutdownCommand>,
    broker_handle: thread::JoinHandle<()>,
    server_shutdown_sender: Option<Sender<ShutdownCommand>>,
    server_handle: Option<thread::JoinHandle<()>>,
    node_shutdown_sender: Sender<ShutdownCommand>,
    node_handle: thread::JoinHandle<()>,
    inv_shutdown_sender: Sender<ShutdownCommand>,
    inv_handle: thread::JoinHandle<()>,
    pow_shutdown_sender: Sender<ShutdownCommand>,
    pow_handle: thread::JoinHandle<()>,
    user_shutdown_sender: Sender<ShutdownCommand>,
    user_handle: thread::JoinHandle<()>,
}

fn accept_loop(
    addr: impl ToSocketAddrs,
    broker: Sender<Event>,
    mut _shutdown: Receiver<ShutdownCommand>,
) -> Result<()> {
    let listener = TcpListener::bind(addr)?;
    let mut incoming = listener.incoming();
    info!("Server started");
    loop {
        let stream = incoming.next();
        if stream.is_none() {
            break;
        }
        let stream = stream.unwrap();
        if let Err(err) = stream {
            error!("{}", err);
            break;
        }
        let stream = Arc::new(stream.unwrap());
        broker.send(Event::Incoming { stream })?;
    }
    info!("Server stopped");
    Ok(())
}

impl ConnectionManager {
    pub fn new(
        config: Config,
        db_path: PathBuf,
        users: Vec<User>,
        bm_event_sender: Sender<BmEvent>,
    ) -> Self {
        let (broker_shutdown_sender, broker_shutdown_receiver) = bounded(1);
        let (broker_sender, broker_receiver) = bounded(config.channel_buffer());
        let (node_shutdown_sender, node_shutdown_receiver) = bounded(1);
        let (node_sender, node_receiver) = bounded(config.channel_buffer());
        let (inv_shutdown_sender, inv_shutdown_receiver) = bounded(1);
        let (inv_sender, inv_receiver) = bounded(config.channel_buffer());
        let (pow_shutdown_sender, pow_shutdown_receiver) = bounded(1);
        let (pow_sender, pow_receiver) = bounded(config.channel_buffer());
        let (user_shutdown_sender, user_shutdown_receiver) = bounded(1);
        let (user_sender, user_receiver) = bounded(config.channel_buffer());

        let ctx = Arc::new(Context {
            config: config.clone(),
            node_nonce: NodeNonce::random(),

            incoming_initiated: AtomicUsize::new(0),
            incoming_connected: AtomicUsize::new(0),
            incoming_established: AtomicUsize::new(0),
            outgoing_initiated: AtomicUsize::new(0),
            outgoing_connected: AtomicUsize::new(0),
            outgoing_established: AtomicUsize::new(0),

            aborted: Arc::new(AtomicBool::new(false)),

            db_path,

            broker_sender,
            bm_event_sender,
            node_sender,
            inv_sender,
            pow_sender,
            user_sender,
        });
        let context = Arc::clone(&ctx);
        let broker_handle =
            thread::spawn(|| broker_loop(context, broker_receiver, broker_shutdown_receiver));

        let server_shutdown_sender;
        let server_handle;
        if let Some(server) = *ctx.config.server() {
            let (shutdown_sender, shutdown_receiver) = bounded(1);
            let bs = ctx.broker_sender.clone();
            let handle = spawn_and_log_error(
                move || accept_loop(server, bs, shutdown_receiver),
                Level::Error,
            );
            server_shutdown_sender = Some(shutdown_sender);
            server_handle = Some(handle);
        } else {
            server_shutdown_sender = None;
            server_handle = None;
        }

        let context = Arc::clone(&ctx);
        let node_handle =
            thread::spawn(|| manage_nodes(context, node_receiver, node_shutdown_receiver));
        let context = Arc::clone(&ctx);
        let inv_handle =
            thread::spawn(|| manage_invs(context, inv_receiver, inv_shutdown_receiver));
        let context = Arc::clone(&ctx);
        let pow_handle =
            thread::spawn(|| manage_pows(context, pow_receiver, pow_shutdown_receiver));
        let context = Arc::clone(&ctx);
        let user_handle =
            thread::spawn(|| manage_users(context, user_receiver, user_shutdown_receiver));

        let node_sender = ctx.node_sender.clone();
        let user_sender = ctx.user_sender.clone();

        let cm = Self {
            ctx,

            broker_shutdown_sender,
            broker_handle,
            server_shutdown_sender,
            server_handle,
            node_shutdown_sender,
            node_handle,
            inv_shutdown_sender,
            inv_handle,
            pow_shutdown_sender,
            pow_handle,
            user_shutdown_sender,
            user_handle,
        };

        thread::spawn(move || {
            let stream_number: StreamNumber = 1.into();
            let now = Time::now();
            let mut list = Vec::with_capacity(config.seeds().len());
            for addr in config.seeds() {
                let entry = NodeEntry::new(stream_number, addr.clone().into(), now);
                list.push(entry);
            }
            for addr in config.bootstraps() {
                let entry = NodeEntry::new(stream_number, addr.clone(), now);
                list.push(entry);
            }
            for addr in config.own_nodes() {
                let entry = NodeEntry::new(stream_number, addr.clone().into(), now);
                list.push(entry);
            }
            if let Err(err) = node_sender.send(NodeEvent::Add(list)) {
                error!("{}", err);
            }

            for user in users {
                if let Err(err) = user_sender.send(UserEvent::User(user)) {
                    error!("{}", err);
                }
            }
        });

        cm
    }

    pub fn stop(self) -> thread::JoinHandle<()> {
        thread::spawn(|| {
            let bm_event_sender = self.ctx.bm_event_sender().clone();

            if let Some(sender) = self.server_shutdown_sender {
                if let Err(err) = sender.send(ShutdownCommand) {
                    error!("{}", err);
                }
            }
            if let Err(err) = self.broker_shutdown_sender.send(ShutdownCommand) {
                error!("{}", err);
            }
            if let Some(server_handle) = self.server_handle {
                if let Err(err) = server_handle.join() {
                    error!("{:?}", err);
                }
            }
            if let Err(err) = self.broker_handle.join() {
                error!("{:?}", err);
            }
            if let Err(err) = self.node_shutdown_sender.send(ShutdownCommand) {
                error!("{}", err);
            }
            if let Err(err) = self.inv_shutdown_sender.send(ShutdownCommand) {
                error!("{}", err);
            }
            if let Err(err) = self.pow_shutdown_sender.send(ShutdownCommand) {
                error!("{}", err);
            }
            if let Err(err) = self.user_shutdown_sender.send(ShutdownCommand) {
                error!("{}", err);
            }
            if let Err(err) = self.node_handle.join() {
                error!("{:?}", err);
            }
            if let Err(err) = self.inv_handle.join() {
                error!("{:?}", err);
            }
            if let Err(err) = self.pow_handle.join() {
                error!("{:?}", err);
            }
            if let Err(err) = self.user_handle.join() {
                error!("{:?}", err);
            }
            if let Err(err) = bm_event_sender.send(BmEvent::Stopped) {
                error!("{}", err);
            }
        })
    }

    pub fn ctx(&self) -> Arc<Context> {
        Arc::clone(&self.ctx)
    }

    pub fn send(&self, header: object::Header, payload: Vec<u8>) -> Result<()> {
        self.ctx
            .pow_sender
            .send(PowEvent::Perform { header, payload })?;
        Ok(())
    }

    pub fn add_identity(&self, id: Vec<u8>, identity: PrivateIdentity) -> Result<()> {
        self.ctx
            .user_sender
            .send(UserEvent::AddIdentity { id, identity })?;
        Ok(())
    }

    pub fn subscribe(&self, id: Vec<u8>, address: Address) -> Result<()> {
        self.ctx
            .user_sender
            .send(UserEvent::Subscribe { id, address })?;
        Ok(())
    }
}

pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Result<T> = std::result::Result<T, Error>;

fn spawn_and_log_error<F>(f: F, level: Level) -> thread::JoinHandle<()>
where
    F: FnOnce() -> Result<()> + Send + 'static,
{
    thread::spawn(move || {
        if let Err(e) = f() {
            log!(level, "{}", e)
        }
    })
}

fn bytes_to_string(bytes: &[u8]) -> String {
    let b: Vec<u8> = bytes
        .iter()
        .map(|c| if *c < 0x20 || *c >= 0x7f { b'.' } else { *c })
        .collect();
    String::from_utf8_lossy(&b).to_string()
}

fn inner_connection_loop(stream: Arc<TcpStream>, conn: &mut Connection) -> Result<()> {
    let reader = &mut BufReader::new(&*stream);

    match conn.direction() {
        Direction::Incoming => {}
        Direction::Outgoing => conn.write_version()?,
    }

    loop {
        let mut bytes = [0; Header::LEN_BM];
        let n = reader.read(&mut bytes[0..1])?;
        if n == 0 {
            break;
        }
        assert_eq!(n, 1);
        reader.read_exact(&mut bytes[1..])?;
        let mut cur = Cursor::new(bytes);
        let header = Header::read_from_with_config(conn.ctx().config().core(), &mut cur)?;

        let mut payload = Vec::new();
        let mut r = reader.take(header.length().as_u32() as u64);
        r.read_to_end(&mut payload)?;

        let packet = Packet::compose(conn.ctx().config().core(), header, payload)?;

        trace!(
            "Read command: {} length: {}",
            bytes_to_string(packet.header().command().as_ref()),
            packet.header().length()
        );

        dispatch_message(conn, packet)?;
    }
    Ok(())
}

#[derive(Debug)]
pub struct TooManyConnectionsError;

impl fmt::Display for TooManyConnectionsError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        "too many connections".fmt(f)
    }
}

impl std::error::Error for TooManyConnectionsError {}

#[allow(clippy::too_many_arguments)]
fn connection_loop(
    broker: Sender<Event>,
    stream: Arc<TcpStream>,
    addr: SocketAddrNode,
    direction: Direction,
    ctx: Arc<Context>,
    bm_event_sender: Sender<BmEvent>,
    node_sender: Sender<NodeEvent>,
    inv_sender: Sender<InvEvent>,
    _shutdown_sender: Sender<Void>,
) -> Result<()> {
    if direction == Direction::Incoming {
        let connected = ctx.connected(direction).load(Ordering::SeqCst);
        if connected >= ctx.config.max_incoming_connected() {
            stream.shutdown(Shutdown::Both)?;
            return Err(Error::from(TooManyConnectionsError));
        }
    }

    let mut conn = Connection::new(
        broker,
        addr.clone(),
        direction,
        Arc::clone(&ctx),
        bm_event_sender.clone(),
        node_sender.clone(),
        inv_sender,
    );

    let connected = ctx.connected(direction).fetch_add(1, Ordering::SeqCst) + 1;
    info!(
        "({:?}) Connected. Current: {} connections",
        direction, connected
    );
    conn.send_bm_event_connection_counts();

    let result = inner_connection_loop(stream, &mut conn);

    let connected = ctx.connected(direction).fetch_sub(1, Ordering::SeqCst) - 1;
    info!(
        "({:?}) Disconnected. Current: {} connections",
        direction, connected
    );
    if conn.established() {
        let established = ctx.established(direction).fetch_sub(1, Ordering::SeqCst) - 1;
        info!("({:?}) Established: {}", direction, established);
    }
    conn.send_bm_event_connection_counts();

    if direction == Direction::Outgoing && !conn.established() {
        if let Err(err) = node_sender.send(NodeEvent::ConnectionFailed(addr.clone())) {
            error!("{}", err);
        }
    }

    if let Err(err) = bm_event_sender.send(BmEvent::Disconnected { addr }) {
        error!("{}", err);
    }

    result
}

fn write_to<'a, W>(packet: &'a impl WriteTo, w: &'a mut W) -> std::io::Result<()>
where
    W: Write,
{
    let mut bytes = Vec::new();
    packet.write_to(&mut bytes).unwrap();
    w.write_all(&bytes)
}

fn connection_writer_loop(
    messages: &mut Receiver<Vec<Packet>>,
    stream: Arc<TcpStream>,
    shutdown: Receiver<Void>,
) -> Result<()> {
    let mut writer = BufWriter::new(&*stream);
    loop {
        select! {
            recv(messages) -> list => match list {
                Ok(list) => {
                    if list.is_empty() {
                        break;
                    }
                    for packet in list {
                        trace!(
                            "Write command: {} length: {}",
                            bytes_to_string(packet.header().command().as_ref()),
                            packet.header().length()
                        );
                        write_to(&packet, &mut writer)?;
                    }
                    writer.flush()?;
                }
                Err(_err) => break,
            },
            recv(shutdown) -> void => match void {
                Ok(void) => match void {},
                Err(_err) => break,
            }
        }
    }
    stream.shutdown(Shutdown::Both)?;
    Ok(())
}

#[derive(Debug)]
pub enum Void {}

#[derive(Debug)]
pub enum Event {
    Incoming {
        stream: Arc<TcpStream>,
    },
    Outgoing {
        addr: SocketAddrNode,
    },
    Write {
        addr: SocketAddrNode,
        list: Vec<Packet>,
    },
    AbortPendings,
    ObjectsNewToMe {
        addr: SocketAddrNode,
        list: Vec<InvHash>,
    },
    GetObjects {
        addr: SocketAddrNode,
        list: Vec<InvHash>,
    },
    ObjectsNewToHer {
        addr: SocketAddrNode,
        list: Vec<InvHash>,
    },
    Established {
        addr: SocketAddrNode,
    },
    Close {
        addr: SocketAddrNode,
    },
    PublishObjects(Vec<InvHash>),
}

#[derive(Debug)]
pub struct ShutdownCommand;

#[allow(clippy::too_many_arguments)]
fn connect_incoming(
    ctx: Arc<Context>,
    addr: SocketAddrNode,
    broker_sender: Sender<Event>,
    bm_event_sender: Sender<BmEvent>,
    node_sender: Sender<NodeEvent>,
    inv_sender: Sender<InvEvent>,
    shutdown_sender: Sender<Void>,
    stream: Arc<TcpStream>,
) -> Result<()> {
    spawn_and_log_error(
        || {
            connection_loop(
                broker_sender,
                stream,
                addr,
                Direction::Incoming,
                ctx,
                bm_event_sender,
                node_sender,
                inv_sender,
                shutdown_sender,
            )
        },
        Level::Debug,
    );
    Ok(())
}

#[derive(Debug)]
pub enum ConnectOutgoingError {
    Error(Error),
    Aborted,
}

impl fmt::Display for ConnectOutgoingError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Error(err) => err.fmt(f),
            Self::Aborted => "aborted".fmt(f),
        }
    }
}

impl std::error::Error for ConnectOutgoingError {}

impl From<socks::ConnectError> for ConnectOutgoingError {
    fn from(err: socks::ConnectError) -> Self {
        ConnectOutgoingError::Error(Error::from(err))
    }
}

#[allow(clippy::too_many_arguments)]
fn connect_outgoing(
    ctx: Arc<Context>,
    addr: SocketAddrNode,
    broker_sender: Sender<Event>,
    bm_event_sender: Sender<BmEvent>,
    node_sender: Sender<NodeEvent>,
    inv_sender: Sender<InvEvent>,
    shutdown_sender: Sender<Void>,
    mut _abort_receiver: Receiver<Void>,
) -> std::result::Result<Arc<TcpStream>, ConnectOutgoingError> {
    let mut stream;
    if let Some(server) = ctx.config.socks() {
        stream = match TcpStream::connect(server) {
            Ok(stream) => stream,
            Err(err) => return Err(ConnectOutgoingError::Error(Error::from(err))),
        };
        if let Some(auth) = ctx.config.socks_auth() {
            let _dest = socks::connect_with_password(
                &mut stream,
                auth.username(),
                auth.password(),
                addr.clone().into(),
            )?;
        } else {
            let _dest = socks::connect(&mut stream, addr.clone().into())?;
        }
    } else {
        match &addr {
            SocketAddrNode::AddrExt(addr) => {
                let server: StdSocketAddr = match addr.clone().try_into() {
                    Ok(addr) => addr,
                    Err(err) => return Err(ConnectOutgoingError::Error(Error::from(err))),
                };
                stream = {
                    let s = TcpStream::connect(server);
                    if let Err(err) = s {
                        return Err(ConnectOutgoingError::Error(Error::from(err)));
                    }
                    s.unwrap()
                };
            }
            SocketAddrNode::Domain(domain) => {
                stream = {
                    let s = TcpStream::connect(domain.to_string());
                    if let Err(err) = s {
                        return Err(ConnectOutgoingError::Error(Error::from(err)));
                    }
                    s.unwrap()
                };
            }
        }
    }
    let stream = Arc::new(stream);
    let s = Arc::clone(&stream);
    let bs = broker_sender;
    spawn_and_log_error(
        || {
            connection_loop(
                bs,
                s,
                addr,
                Direction::Outgoing,
                ctx,
                bm_event_sender,
                node_sender,
                inv_sender,
                shutdown_sender,
            )
        },
        Level::Debug,
    );
    Ok(stream)
}

fn send_bm_event_connection_counts(ctx: &Context, bm_event_sender: &mut Sender<BmEvent>) {
    if let Err(err) = bm_event_sender.send(BmEvent::ConnectionCounts {
        incoming_initiated: ctx.initiated(Direction::Incoming).load(Ordering::SeqCst),
        incoming_connected: ctx.connected(Direction::Incoming).load(Ordering::SeqCst),
        incoming_established: ctx.established(Direction::Incoming).load(Ordering::SeqCst),
        outgoing_initiated: ctx.initiated(Direction::Outgoing).load(Ordering::SeqCst),
        outgoing_connected: ctx.connected(Direction::Outgoing).load(Ordering::SeqCst),
        outgoing_established: ctx.established(Direction::Outgoing).load(Ordering::SeqCst),
    }) {
        error!("{}", err);
    }
}

fn broker_loop(
    ctx: Arc<Context>,
    events: Receiver<Event>,
    shutdown_receiver: Receiver<ShutdownCommand>,
) {
    let broker_sender = ctx.broker_sender.clone();
    let mut bm_event_sender = ctx.bm_event_sender.clone();
    let node_sender = ctx.node_sender.clone();
    let inv_sender = ctx.inv_sender.clone();

    let (disconnect_sender, disconnect_receiver) =
        bounded::<(SocketAddrNode, Receiver<Vec<Packet>>)>(ctx.config().channel_buffer());
    let mut peers: HashMap<SocketAddrNode, Sender<Vec<Packet>>> = HashMap::new();
    let mut abort_senders: HashMap<SocketAddrNode, Sender<Void>> = HashMap::new();
    let mut objects_new_to_me: HashMap<SocketAddrNode, Vec<InvHash>> = HashMap::new();
    let mut pending_uploads: HashMap<SocketAddrNode, HashSet<InvHash>> = HashMap::new();
    let mut objects_new_to_her: HashMap<SocketAddrNode, HashMap<InvHash, Time>> = HashMap::new();
    let mut established: HashSet<SocketAddrNode> = HashSet::new();

    let transfer_interval = crossbeam_channel::tick(StdDuration::from_secs(1));
    let clean_objects_new_to_her_interval = crossbeam_channel::tick(StdDuration::from_secs(300));
    let ping_interval = crossbeam_channel::tick(StdDuration::from_secs(120));
    loop {
        let event = select! {
            recv(events) -> event => match event {
                Err(_err) => break,
                Ok(event) => event,
            },
            recv(shutdown_receiver) -> _event => break,
            recv(disconnect_receiver) -> disconnect => {
                let (addr, _pending_messages) = disconnect.unwrap();
                assert!(peers.remove(&addr).is_some());
                if abort_senders.remove(&addr).is_some() {
                    ctx.initiated(Direction::Outgoing).fetch_sub(1, Ordering::SeqCst);
                } else {
                    ctx.initiated(Direction::Incoming).fetch_sub(1, Ordering::SeqCst);
                }
                objects_new_to_me.remove(&addr);
                pending_uploads.remove(&addr);
                objects_new_to_her.remove(&addr);
                established.remove(&addr);
                if let Err(err) = node_sender.send(NodeEvent::Disconnected(addr.clone())) {
                    error!("{}", err);
                }
                send_bm_event_connection_counts(&ctx, &mut bm_event_sender);
                continue;
            },
            recv(transfer_interval) -> tick => match tick {
                Ok(_) => {
                    for (addr, vec) in &mut objects_new_to_me {
                        if !established.contains(&addr) {
                            continue;
                        }
                        if vec.is_empty() {
                            continue;
                        }
                        vec.shuffle(&mut rand::thread_rng());
                        const MAX_COUNT: usize = 1000;
                        let count = {
                            let mut iter = vec.chunks(MAX_COUNT);
                            if let Some(chunk) = iter.next() {
                                let getdata = message::Getdata::new(chunk.to_vec()).unwrap();
                                let packet = getdata.pack(ctx.config().core()).unwrap();
                                if let Err(err) = broker_sender.send(
                                    Event::Write { addr: addr.clone(), list: vec![packet] }) {
                                    error!("{}", err);
                                }
                                chunk.len()
                            } else {
                                0
                            }
                        };
                        vec.drain(0..count);
                    }

                    for (addr, set) in &mut pending_uploads {
                        if !established.contains(&addr) {
                            continue;
                        }
                        if set.is_empty() {
                            continue;
                        }
                        let mut vec: Vec<InvHash> = set.iter().cloned().collect();
                        vec.shuffle(&mut rand::thread_rng());
                        const MAX_COUNT: usize = 1000;
                        let count = {
                            let mut iter = vec.chunks(MAX_COUNT);
                            if let Some(chunk) = iter.next() {
                                if let Err(err) = inv_sender.send(
                                    InvEvent::SendObjects { addr: addr.clone(), list: chunk.to_vec() }
                                ) {
                                    error!("{}", err);
                                }
                                chunk.len()
                            } else {
                                0
                            }
                        };
                        for hash in vec[0..count].iter() {
                            set.remove(&hash);
                        }
                    }

                    for (addr, map) in &mut objects_new_to_her {
                        if !established.contains(&addr) {
                            continue;
                        }
                        if map.is_empty() {
                            continue;
                        }
                        let mut vec: Vec<InvHash> = map.keys().cloned().collect();
                        vec.shuffle(&mut rand::thread_rng());
                        const MAX_COUNT: usize = 1000;
                        let mut iter = vec.chunks(MAX_COUNT);
                        if let Some(chunk) = iter.next() {
                            let inv = message::Inv::new(chunk.to_vec()).unwrap();
                            let packet = inv.pack(ctx.config().core()).unwrap();
                            if let Err(err) = broker_sender
                                .send(Event::Write {
                                    addr: addr.clone(),
                                    list: vec![packet],
                                })
                            {
                                error!("{}", err);
                            }

                            for hash in chunk {
                                map.remove(&hash);
                            }
                        }
                    }
                    continue;
                },
                Err(_err) => break,
            },
            recv(clean_objects_new_to_her_interval) -> tick => match tick {
                Ok(_) => {
                    let now = Time::now();
                    for map in objects_new_to_her.values_mut() {
                        map.retain(|_hash, time| {
                            match time.checked_add(REQUEST_EXPIRES) {
                                Some(target) => now < target,
                                None => false,
                            }
                        });
                    }
                    continue;
                },
                Err(_err) => break,
            },
            recv(ping_interval) -> tick => match tick {
                Ok(_) => {
                    let packet = message::Ping::new().pack(ctx.config().core()).unwrap();
                    for addr in &established {
                        if let Err(err) = broker_sender.send(
                            Event::Write { addr: addr.clone(), list: vec![packet.clone()]}
                        ) {
                            error!("{}", err);
                        }
                    }
                    continue;
                },
                Err(_err) => break,
            },
        };
        match event {
            Event::Incoming { stream } => {
                if let Ok(addr) = stream.peer_addr() {
                    let addr: SocketAddrExt = addr.into();
                    let addr: SocketAddrNode = addr.into();
                    match peers.entry(addr.clone()) {
                        Entry::Occupied(..) => (),
                        Entry::Vacant(entry) => {
                            let ctx = Arc::clone(&ctx);
                            let addr = addr.clone();
                            let broker_sender = broker_sender.clone();
                            let mut bm_event_sender = bm_event_sender.clone();
                            let node_sender = node_sender.clone();
                            let inv_sender = inv_sender.clone();
                            let disconnect_sender = disconnect_sender.clone();
                            let (client_sender, mut client_receiver) =
                                bounded(ctx.config().channel_buffer());
                            entry.insert(client_sender);
                            objects_new_to_me.insert(addr.clone(), Vec::new());
                            pending_uploads.insert(addr.clone(), HashSet::new());
                            objects_new_to_her.insert(addr.clone(), HashMap::new());
                            ctx.initiated(Direction::Incoming)
                                .fetch_add(1, Ordering::SeqCst);
                            send_bm_event_connection_counts(&ctx, &mut bm_event_sender);
                            spawn_and_log_error(
                                move || {
                                    let (shutdown_sender, shutdown_receiver) =
                                        bounded::<Void>(ctx.config().channel_buffer());
                                    let mut e: Option<Error> = None;
                                    match connect_incoming(
                                        ctx,
                                        addr.clone(),
                                        broker_sender,
                                        bm_event_sender,
                                        node_sender,
                                        inv_sender,
                                        shutdown_sender,
                                        Arc::clone(&stream),
                                    ) {
                                        Ok(_) => {
                                            if let Err(err) = connection_writer_loop(
                                                &mut client_receiver,
                                                stream,
                                                shutdown_receiver,
                                            ) {
                                                e = Some(err)
                                            }
                                        }
                                        Err(err) => e = Some(err),
                                    }
                                    disconnect_sender.send((addr, client_receiver)).unwrap();
                                    if let Some(err) = e {
                                        return Err(err);
                                    }
                                    Ok(())
                                },
                                Level::Debug,
                            );
                        }
                    }
                }
            }
            Event::Outgoing { addr } => {
                if !ctx.config.is_connectable_to(&addr) {
                    continue;
                }
                match peers.entry(addr.clone()) {
                    Entry::Occupied(..) => (),
                    Entry::Vacant(entry) => {
                        let ctx = Arc::clone(&ctx);
                        let addr = addr.clone();
                        let broker_sender = broker_sender.clone();
                        let mut bm_event_sender = bm_event_sender.clone();
                        let node_sender = node_sender.clone();
                        let inv_sender = inv_sender.clone();
                        let disconnect_sender = disconnect_sender.clone();
                        let (client_sender, mut client_receiver) =
                            bounded(ctx.config().channel_buffer());
                        entry.insert(client_sender);
                        let (abort_sender, abort_receiver) =
                            bounded::<Void>(ctx.config().channel_buffer());
                        abort_senders.insert(addr.clone(), abort_sender);
                        objects_new_to_me.insert(addr.clone(), Vec::new());
                        pending_uploads.insert(addr.clone(), HashSet::new());
                        objects_new_to_her.insert(addr.clone(), HashMap::new());
                        ctx.initiated(Direction::Outgoing)
                            .fetch_add(1, Ordering::SeqCst);
                        send_bm_event_connection_counts(&ctx, &mut bm_event_sender);
                        spawn_and_log_error(
                            move || {
                                let (shutdown_sender, shutdown_receiver) =
                                    bounded::<Void>(ctx.config().channel_buffer());
                                let mut e: Option<Error> = None;
                                match connect_outgoing(
                                    ctx,
                                    addr.clone(),
                                    broker_sender,
                                    bm_event_sender,
                                    node_sender.clone(),
                                    inv_sender,
                                    shutdown_sender,
                                    abort_receiver,
                                ) {
                                    Ok(stream) => {
                                        if let Err(err) = connection_writer_loop(
                                            &mut client_receiver,
                                            stream,
                                            shutdown_receiver,
                                        ) {
                                            e = Some(err)
                                        }
                                    }
                                    Err(err) => match err {
                                        ConnectOutgoingError::Error(err) => {
                                            if let Err(err) = node_sender
                                                .send(NodeEvent::ConnectionFailed(addr.clone()))
                                            {
                                                error!("{}", err);
                                            }
                                            e = Some(err)
                                        }
                                        ConnectOutgoingError::Aborted => e = Some(Error::from(err)),
                                    },
                                }
                                disconnect_sender.send((addr, client_receiver)).unwrap();
                                if let Some(err) = e {
                                    return Err(err);
                                }
                                Ok(())
                            },
                            Level::Debug,
                        );
                    }
                }
            }
            Event::Write { addr, list } => {
                if let Some(peer) = peers.get_mut(&addr) {
                    if let Err(err) = peer.send(list) {
                        error!("{}", err);
                    }
                }
            }
            Event::AbortPendings => {
                // omitted
                /*
                for sender in abort_senders.values_mut() {
                    if let Err(err) = sender.close() {
                        error!("{}", err);
                    }
                }
                */
            }
            Event::ObjectsNewToMe { addr, mut list } => {
                if let Some(vec) = objects_new_to_me.get_mut(&addr) {
                    vec.append(&mut list);
                }
            }
            Event::GetObjects { addr, list } => {
                if let Some(set) = pending_uploads.get_mut(&addr) {
                    for hash in list {
                        set.insert(hash);
                    }
                }
            }
            Event::ObjectsNewToHer { addr, list } => {
                let now = Time::now();
                for (a, map) in &mut objects_new_to_her {
                    if *a == addr {
                        continue;
                    }
                    for hash in &list {
                        map.insert(hash.clone(), now);
                    }
                }
            }
            Event::Established { addr } => {
                established.insert(addr);
            }
            Event::Close { addr } => {
                if let Some(peer) = peers.get_mut(&addr) {
                    if let Err(err) = peer.send(Vec::with_capacity(0)) {
                        error!("{}", err);
                    }
                }
            }
            Event::PublishObjects(list) => {
                let now = Time::now();
                for map in objects_new_to_her.values_mut() {
                    for hash in &list {
                        map.insert(hash.clone(), now);
                    }
                }
            }
        }
    }

    debug!("abort_senders: {}", abort_senders.len());
    drop(abort_senders);
    ctx.initiated(Direction::Outgoing)
        .store(0, Ordering::SeqCst);
    debug!("peers: {}", peers.len());
    drop(peers);
    drop(objects_new_to_me);
    drop(pending_uploads);
    drop(objects_new_to_her);
    drop(established);
    drop(disconnect_sender);
    ctx.initiated(Direction::Incoming)
        .store(0, Ordering::SeqCst);
    while let Ok((_name, _pending_messages)) = disconnect_receiver.recv() {}
}
