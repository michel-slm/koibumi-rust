//! Types for handling bootstrap domain names.

use std::{convert::TryFrom, fmt, str::FromStr};

use serde::{de, Deserialize, Deserializer, Serialize, Serializer};

use koibumi_core::net::SocketAddrExt;
use koibumi_net::{
    domain::{ParseSocketDomainError, SocketDomain},
    socks::SocketAddr as SocksSocketAddr,
};

/// A bootstrap socket address or an extended socket address.
#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum SocketAddrNode {
    /// An extended socket address.
    AddrExt(SocketAddrExt),
    /// A socket domain name.
    Domain(SocketDomain),
}

impl fmt::Display for SocketAddrNode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::AddrExt(addr) => addr.fmt(f),
            Self::Domain(domain) => domain.fmt(f),
        }
    }
}

impl Serialize for SocketAddrNode {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl<'de> Deserialize<'de> for SocketAddrNode {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        s.parse().map_err(de::Error::custom)
    }
}

impl From<SocketAddrExt> for SocketAddrNode {
    fn from(addr: SocketAddrExt) -> Self {
        Self::AddrExt(addr)
    }
}

impl From<SocketDomain> for SocketAddrNode {
    fn from(domain: SocketDomain) -> Self {
        Self::Domain(domain)
    }
}

/// The error type returned when a conversion from a Bootstrap socket domain name
/// to a SOCKS socket address fails.
///
/// This error is used as the error type for the `TryFrom` implementation
/// for `SocksSocketAddr`.
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum TryFromSocketAddrNodeError {
    /// Could not convert the socket domain name to an extended socket address.
    Domain(SocketDomain),
}

impl fmt::Display for TryFromSocketAddrNodeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Domain(domain) => write!(
                f,
                "could not convert {} to an extended socket address",
                domain
            ),
        }
    }
}

impl std::error::Error for TryFromSocketAddrNodeError {}

impl TryFrom<SocketAddrNode> for SocketAddrExt {
    type Error = TryFromSocketAddrNodeError;

    fn try_from(addr: SocketAddrNode) -> Result<Self, <Self as TryFrom<SocketAddrNode>>::Error> {
        match addr {
            SocketAddrNode::AddrExt(addr) => Ok(addr),
            SocketAddrNode::Domain(domain) => Err(TryFromSocketAddrNodeError::Domain(domain)),
        }
    }
}

/// An error which can be returned
/// when parsing a socket address for a node.
///
/// This error is used as the error type for the `FromStr` implementation
/// for [`SocketAddrNode`].
///
/// [`SocketAddrNode`]: enum.SocketAddrNode.html
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ParseSocketAddrNodeError {
    /// An error was caught when parsing a socket domain name.
    /// The actual error caught is returned
    /// as a payload of this variant.
    ParseSocketDomainError(ParseSocketDomainError),
}

impl fmt::Display for ParseSocketAddrNodeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::ParseSocketDomainError(err) => err.fmt(f),
        }
    }
}

impl std::error::Error for ParseSocketAddrNodeError {}

impl From<ParseSocketDomainError> for ParseSocketAddrNodeError {
    fn from(err: ParseSocketDomainError) -> Self {
        Self::ParseSocketDomainError(err)
    }
}

impl FromStr for SocketAddrNode {
    type Err = ParseSocketAddrNodeError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(addr) = s.parse::<SocketAddrExt>() {
            return Ok(Self::AddrExt(addr));
        }
        Ok(Self::Domain(s.parse::<SocketDomain>()?))
    }
}

#[test]
fn test_socket_addr_node_from_str() {
    use koibumi_net::domain::Domain;

    let test = "example.org:1234".parse::<SocketAddrNode>().unwrap();
    let expected = SocketAddrNode::Domain(SocketDomain::new(
        Domain::new("example.org").unwrap(),
        1234.into(),
    ));
    assert_eq!(test, expected);
}

impl From<SocketAddrNode> for SocksSocketAddr {
    fn from(addr: SocketAddrNode) -> Self {
        match addr {
            SocketAddrNode::AddrExt(addr) => addr.into(),
            SocketAddrNode::Domain(domain) => domain.into(),
        }
    }
}
