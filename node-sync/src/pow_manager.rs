use std::{
    collections::HashMap,
    io::Cursor,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread,
    time::Duration as StdDuration,
};

use crossbeam_channel::{bounded, select, Receiver};
use log::{error, info};

use koibumi_core::{
    hash::{sha512, Hash512},
    io::{LenBm, ReadFrom, WriteTo},
    message, object, pow,
    time::Time,
};

use crate::{
    connection_loop::{Context, ShutdownCommand},
    inv_manager::Event as InvEvent,
};

#[derive(Debug)]
pub enum Event {
    Perform {
        header: object::Header,
        payload: Vec<u8>,
    },
    Done(message::Object),
}

fn initial_hash(payload: impl AsRef<[u8]>) -> Hash512 {
    sha512(payload)
}

fn perform_pow(
    ctx: Arc<Context>,
    payload: impl AsRef<[u8]>,
    expires_time: Time,
) -> Result<pow::Nonce, pow::PerformError> {
    let cpus = num_cpus::get_physical().max(1);

    let cancel = Arc::new(AtomicBool::new(false));
    let (sender, receiver) = bounded::<Result<pow::Nonce, pow::PerformError>>(cpus + 1);
    for i in 0..cpus {
        let sender = sender.clone();
        let ctx = Arc::clone(&ctx);
        let len = payload.as_ref().len();
        let initial_hash = pow::initial_hash(payload.as_ref());
        let nonce_trials_per_byte = ctx.config().core().nonce_trials_per_byte();
        let payload_length_extra_bytes = ctx.config().core().payload_length_extra_bytes();
        let cancel = Arc::clone(&cancel);
        thread::spawn(move || {
            let target = pow::target(
                ctx.config().core(),
                len,
                expires_time,
                Time::now(),
                nonce_trials_per_byte,
                payload_length_extra_bytes,
            );
            let result = pow::perform(
                initial_hash,
                target,
                (i as u64).into(),
                cpus,
                Arc::clone(&cancel),
            );
            if let Err(err) = sender.send(result) {
                error!("{}", err);
            }
            cancel.store(true, Ordering::SeqCst);
        });
    }
    {
        let aborted = Arc::clone(ctx.aborted());
        let cancel = Arc::clone(&cancel);
        thread::spawn(move || {
            loop {
                if aborted.load(Ordering::SeqCst) {
                    break;
                }
                if cancel.load(Ordering::SeqCst) {
                    break;
                }
                thread::sleep(StdDuration::from_secs(1));
            }
            cancel.store(true, Ordering::SeqCst);
            if let Err(err) = sender.send(Err(pow::PerformError::Canceled)) {
                error!("{}", err);
            }
        });
    }

    match receiver.recv() {
        Ok(result) => result,
        Err(_err) => Err(pow::PerformError::Canceled),
    }
}

pub fn manage(
    ctx: Arc<Context>,
    receiver: Receiver<Event>,
    shutdown_receiver: Receiver<ShutdownCommand>,
) {
    let conn = rusqlite::Connection::open(ctx.db_path());
    if let Err(err) = conn {
        error!("{}", err);
        return;
    }
    let conn = conn.unwrap();

    let sender = ctx.pow_sender().clone();
    let inv_sender = ctx.inv_sender().clone();

    if let Err(err) = conn.execute(
        "CREATE TABLE IF NOT EXISTS pendings (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            hash BLOB NOT NULL,
            header BLOB NOT NULL,
            payload BLOB NOT NULL
        )",
        rusqlite::params![],
    ) {
        error!("{}", err);
    }

    if let Ok(mut stmt) = conn.prepare("SELECT header, payload FROM pendings ORDER BY id") {
        if let Ok(list) = stmt.query_map(rusqlite::params![], |row| {
            Ok((row.get::<usize, Vec<u8>>(0)?, row.get::<usize, Vec<u8>>(1)?))
        }) {
            for elem in list {
                if let Err(err) = elem {
                    error!("{}", err);
                    continue;
                }
                let elem = elem.unwrap();
                let header = {
                    let mut r = Cursor::new(elem.0);
                    object::Header::read_from(&mut r)
                };
                if let Err(err) = header {
                    error!("{}", err);
                    continue;
                }
                if let Err(err) = sender.send(Event::Perform {
                    header: header.unwrap(),
                    payload: elem.1,
                }) {
                    error!("{}", err);
                }
            }
        }
    }

    let mut handles: HashMap<Hash512, thread::JoinHandle<()>> = HashMap::new();

    loop {
        select! {
            recv(receiver) -> event => match event {
                Ok(event) => {
                    match event {
                        Event::Perform { header, payload } => {
                            info!("PoW start");
                            let mut bytes = Vec::with_capacity(header.len_bm() as usize + payload.len());
                            header.write_to(&mut bytes).unwrap();
                            payload.write_to(&mut bytes).unwrap();
                            let hash = initial_hash(&bytes);
                            let mut header_bytes = Vec::with_capacity(header.len_bm());
                            header.write_to(&mut header_bytes).unwrap();
                            if let Ok(mut stmt) = conn.prepare("SELECT header, payload FROM pendings WHERE hash=?1")
                            {
                                if let Ok(mut list) = stmt.query_map(rusqlite::params![hash.as_ref()], |row| {
                                    Ok((row.get::<usize, Vec<u8>>(0)?, row.get::<usize, Vec<u8>>(1)?))
                                }) {
                                    if list.next() == None {
                                        if let Err(err) = conn.execute(
                                            "INSERT INTO pendings (
                                                    hash, header, payload
                                                ) VALUES (?1, ?2, ?3)",
                                            rusqlite::params![hash.as_ref(), header_bytes, &payload],
                                        ) {
                                            error!("{}", err);
                                        }
                                    }
                                }
                            }
                            let ctx = Arc::clone(&ctx);
                            let expires = header.expires_time();
                            let sender = sender.clone();
                            let handle = thread::spawn(move || {
                                let nonce = perform_pow(ctx, &bytes, expires);
                                if let Err(err) = nonce {
                                    error!("{}", err);
                                    return;
                                }
                                let nonce = nonce.unwrap();
                                match message::Object::new(nonce, header, payload) {
                                    Ok(message) => {
                                        if let Err(err) = sender.send(Event::Done(message)) {
                                            error!("{}", err);
                                        }
                                    }
                                    Err(err) => {
                                        error!("{}", err);
                                        // TODO cancel
                                    }
                                }
                            });
                            handles.insert(hash, handle);
                        }
                        Event::Done(message) => {
                            let mut bytes = Vec::with_capacity(
                                message.header().len_bm() as usize + message.object_payload().len(),
                            );
                            message.header().write_to(&mut bytes).unwrap();
                            message.object_payload().write_to(&mut bytes).unwrap();
                            let hash = initial_hash(bytes);
                            if let Err(err) = inv_sender.send(InvEvent::Insert(message)) {
                                error!("{}", err);
                            } else if let Err(err) = conn.execute(
                                "DELETE FROM pendings WHERE hash=?1",
                                rusqlite::params![hash.as_ref()],
                            ) {
                                error!("{}", err);
                            }
                            handles.remove(&hash);
                            info!("PoW done");
                        }
                    }
                },
                Err(_err) => break,
            },
            recv(shutdown_receiver) -> _v => break,
        }
    }

    for (_hash, handle) in handles {
        if let Err(err) = handle.join() {
            error!("{:?}", err);
        }
    }
}
