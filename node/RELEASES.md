# Version 0.0.8 (2021-01-)

* Added `deny(unsafe_code)` directive.

# Version 0.0.7 (2020-09-09)

* Added a bootstrap socket address type.

# Version 0.0.6 (2020-09-07)

* Added the authentication option for SOCKS5.
* Now handles msg objects.

# Version 0.0.5 (2020-08-25)

* Fixed a PoW bug generating incorrect nonce.

# Version 0.0.4 (2020-08-17)

* Now the node receives broadcast messages.

# Version 0.0.3 (2020-07-28)

* Now the node handles onionpeer Bitmessage objects.
* Now the node advertises its own node addresses.

# Version 0.0.2 (2020-06-09)

* Now the configration object has addresses of seed nodes.
* Now known addresses are sent to the peers when connected.
* Now downloaded objects and list of known node addresses are persistent.

# Version 0.0.1 (2020-05-26)

* Bumped the version due to the update of `koibumi-socks`.

# Version 0.0.0 (2020-05-23)

* Features
    * Connect to the Bitmessage network via Tor SOCKS5 proxy
    * Relay Bitmessage objects

Limitations:

* User identities are not recognized.
* Known addresses are not advertised.
