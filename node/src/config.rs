use std::net::SocketAddr as StdSocketAddr;

use serde::{Deserialize, Serialize};

use koibumi_core::{
    message::{StreamNumbers, UserAgent},
    net::SocketAddrExt,
    Config as CoreConfig,
};

use crate::net::SocketAddrNode;

/// Represents a username and a password used to authenticate SOCKS5 connection.
#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct SocksAuth {
    username: String,
    password: String,
}

impl SocksAuth {
    /// Creates a authentication object consists of a username and a password.
    pub fn new(username: String, password: String) -> Self {
        Self { username, password }
    }

    /// Returns the username.
    pub fn username(&self) -> &str {
        &self.username
    }

    /// Returns the password.
    pub fn password(&self) -> &str {
        &self.password
    }
}

/// A set of configurations for a Koibumi Bitmessage node.
#[derive(Clone, PartialEq, Eq, Debug, Serialize, Deserialize)]
pub struct Config {
    #[serde(default = "default_channel_buffer")]
    channel_buffer: usize,

    #[serde(default = "default_server")]
    server: Option<StdSocketAddr>,
    #[serde(default = "default_socks")]
    socks: Option<StdSocketAddr>,
    #[serde(default = "default_connect_to_onion")]
    connect_to_onion: bool,
    #[serde(default = "default_connect_to_ip")]
    connect_to_ip: bool,
    #[serde(default = "default_connect_to_myself")]
    connect_to_myself: bool,
    #[serde(default = "default_user_agent")]
    user_agent: UserAgent,
    #[serde(default = "default_stream_numbers")]
    stream_numbers: StreamNumbers,

    #[serde(default = "default_seeds")]
    seeds: Vec<SocketAddrExt>,
    #[serde(default = "default_bootstraps")]
    bootstraps: Vec<SocketAddrNode>,

    #[serde(default = "default_max_incoming_connected")]
    max_incoming_connected: usize,
    #[serde(default = "default_max_incoming_established")]
    max_incoming_established: usize,
    #[serde(default = "default_max_outgoing_initiated")]
    max_outgoing_initiated: usize,
    #[serde(default = "default_max_outgoing_established")]
    max_outgoing_established: usize,

    #[serde(default = "default_max_nodes")]
    max_nodes: usize,

    #[serde(default = "default_own_nodes")]
    own_nodes: Vec<SocketAddrExt>,

    #[serde(default = "default_socks_auth")]
    socks_auth: Option<SocksAuth>,

    #[serde(default = "default_core")]
    core: CoreConfig,
}

fn default_core() -> CoreConfig {
    CoreConfig::default()
}

fn default_channel_buffer() -> usize {
    0x10000
}

fn default_server() -> Option<StdSocketAddr> {
    None
}

fn default_socks() -> Option<StdSocketAddr> {
    None
}

fn default_socks_auth() -> Option<SocksAuth> {
    None
}

fn default_connect_to_onion() -> bool {
    false
}

fn default_connect_to_ip() -> bool {
    false
}

fn default_connect_to_myself() -> bool {
    false
}

fn default_user_agent() -> UserAgent {
    DEFAULT_USER_AGENT_STR.to_vec().into()
}

fn default_stream_numbers() -> StreamNumbers {
    vec![1_u32.into()].into()
}

fn default_seeds() -> Vec<SocketAddrExt> {
    Vec::with_capacity(0)
}

fn default_bootstraps() -> Vec<SocketAddrNode> {
    Vec::with_capacity(0)
}

fn default_max_incoming_connected() -> usize {
    160
}

fn default_max_incoming_established() -> usize {
    128
}

fn default_max_outgoing_initiated() -> usize {
    32
}

fn default_max_outgoing_established() -> usize {
    8
}

fn default_max_nodes() -> usize {
    20000
}

fn default_own_nodes() -> Vec<SocketAddrExt> {
    Vec::with_capacity(0)
}

impl Default for Config {
    fn default() -> Self {
        Self {
            core: default_core(),
            channel_buffer: default_channel_buffer(),

            server: default_server(),
            socks: default_socks(),
            socks_auth: default_socks_auth(),
            connect_to_onion: default_connect_to_onion(),
            connect_to_ip: default_connect_to_ip(),
            connect_to_myself: default_connect_to_myself(),
            user_agent: default_user_agent(),
            stream_numbers: default_stream_numbers(),

            seeds: default_seeds(),
            bootstraps: default_bootstraps(),

            max_incoming_connected: default_max_incoming_connected(),
            max_incoming_established: default_max_incoming_established(),
            max_outgoing_initiated: default_max_outgoing_initiated(),
            max_outgoing_established: default_max_outgoing_established(),

            max_nodes: default_max_nodes(),

            own_nodes: default_own_nodes(),
        }
    }
}

impl Config {
    /// Constructs a builder for building a configuration set.
    pub fn builder() -> Builder {
        Builder::new()
    }

    /// Constructs a default configuration set.
    pub fn new() -> Self {
        Self::default()
    }

    /// Returns the core configuration set
    /// which this configuration set contains.
    /// The default is the default of the core configuration set.
    pub fn core(&self) -> &CoreConfig {
        &self.core
    }

    /// Returns the buffer size of channels
    /// which will be created in this node.
    /// The default is `0x10000`.
    pub fn channel_buffer(&self) -> usize {
        self.channel_buffer
    }

    /// Returns the `Option` of the socket address of the server
    /// which listens incomming connections.
    /// If this is `None`, no server will be launched.
    /// The default is `None`.
    pub fn server(&self) -> &Option<StdSocketAddr> {
        &self.server
    }

    /// Returns the `Option` of the socket address of the SOCKS5 server
    /// which is used by outgoing connections.
    /// If this is `None`, connections are directly to Clearnet.
    /// The default is `None`.
    pub fn socks(&self) -> &Option<StdSocketAddr> {
        &self.socks
    }

    /// Returns the `Option` of the authentication method of the SOCKS5
    /// which is used by outgoing connections.
    /// The default is `None`.
    pub fn socks_auth(&self) -> &Option<SocksAuth> {
        &self.socks_auth
    }

    /// Returns the flag whether this node connects to remote nodes
    /// that have Onion addresses.
    /// Only outgoing connections are affected.
    /// The default is `false`.
    pub fn connect_to_onion(&self) -> bool {
        self.connect_to_onion
    }

    /// Returns the flag whether this node connects to remote nodes
    /// that have IP addresses.
    /// Only outgoing connections are affected.
    /// The default is `false`.
    pub fn connect_to_ip(&self) -> bool {
        self.connect_to_ip
    }

    /// Returns whether this node can connect to the node
    /// specified by the socket address.
    /// It is affected by the type of the address
    /// which is Onion or IP.
    pub fn is_connectable_to(&self, addr: &SocketAddrNode) -> bool {
        match addr {
            SocketAddrNode::AddrExt(addr) => match addr {
                SocketAddrExt::Ipv4(_addr) => self.connect_to_ip,
                SocketAddrExt::Ipv6(_addr) => self.connect_to_ip,
                SocketAddrExt::OnionV2(_addr) => self.connect_to_onion,
                SocketAddrExt::OnionV3(_addr) => self.connect_to_onion,
            },
            SocketAddrNode::Domain(_domain) => self.connect_to_ip,
        }
    }

    /// Returns the flag whether this node can connect to this node itself.
    /// The default is `false`.
    pub fn connect_to_myself(&self) -> bool {
        self.connect_to_myself
    }

    /// Returns the user agent which is sent to the nodes
    /// this node connected to.
    /// The default is `"/PyBitmessage:0.6.3.2/"`.
    pub fn user_agent(&self) -> &UserAgent {
        &self.user_agent
    }

    /// Returns the set of stream numbers
    /// that this node is interested in.
    /// The default is `[1]`.
    pub fn stream_numbers(&self) -> &StreamNumbers {
        &self.stream_numbers
    }

    /// Returns the list of socket addresses of the seed nodes.
    /// The default is `[]`.
    pub fn seeds(&self) -> &[SocketAddrExt] {
        &self.seeds
    }

    /// Returns the list of socket addresses of the bootstrap domain names.
    /// The default is `[]`.
    pub fn bootstraps(&self) -> &[SocketAddrNode] {
        &self.bootstraps
    }

    /// Returns the maximum number of incoming connections
    /// this node accepts.
    /// The default is `160`.
    pub fn max_incoming_connected(&self) -> usize {
        self.max_incoming_connected
    }

    /// Returns the maximum number of incoming established connections
    /// this node accepts.
    /// The default is `128`.
    pub fn max_incoming_established(&self) -> usize {
        self.max_incoming_established
    }

    /// Returns the maximum number of outgoing conections
    /// this node initiates.
    /// The default is `32`.
    pub fn max_outgoing_initiated(&self) -> usize {
        self.max_outgoing_initiated
    }

    /// Returns the maximum number of outgoing established connections
    /// this node keeps.
    /// The default is `8`.
    pub fn max_outgoing_established(&self) -> usize {
        self.max_outgoing_established
    }

    /// Returns the maximum number of node addresses
    /// this node memorizes.
    /// The default is `20000`.
    pub fn max_nodes(&self) -> usize {
        self.max_nodes
    }

    /// Returns the list of the socket addresses of the own node.
    /// The default is `[]`.
    pub fn own_nodes(&self) -> &[SocketAddrExt] {
        &self.own_nodes
    }
}

/// A builder for building a configuration set
/// for a Koibumi Bitmessage node.
#[derive(Clone, PartialEq, Eq, Debug, Default)]
pub struct Builder {
    config: Config,
}

const DEFAULT_USER_AGENT_STR: &[u8] = b"/PyBitmessage:0.6.3.2/";

impl Builder {
    fn new() -> Self {
        Self::default()
    }

    /// Sets the core configuration set
    /// which the configuration set this builder builds contains.
    /// The default is the default of the core configuration set.
    pub fn core(&mut self, core: CoreConfig) -> &mut Self {
        self.config.core = core;
        self
    }

    /// Sets the buffer size of channels
    /// which created in this node.
    /// The default is `0x10000`.
    pub fn channel_buffer(&mut self, v: usize) -> &mut Self {
        self.config.channel_buffer = v;
        self
    }

    /// Sets the `Option` of the socket address of the server
    /// which listens incomming connections.
    /// If this is `None`, no server will be launched.
    /// The default is `None`.
    pub fn server(&mut self, addr: Option<StdSocketAddr>) -> &mut Self {
        self.config.server = addr;
        self
    }

    /// Sets the `Option` of the socket address of the SOCKS5 server
    /// which is used by outgoing connections.
    /// If this is `None`, connections are directly to Clearnet.
    /// The default is `None`.
    pub fn socks(&mut self, addr: Option<StdSocketAddr>) -> &mut Self {
        self.config.socks = addr;
        self
    }

    /// Sets the `Option` of the authentication method of the SOCKS5
    /// which is used by outgoing connections.
    /// The default is `None`.
    pub fn socks_auth(&mut self, auth: Option<SocksAuth>) -> &mut Self {
        self.config.socks_auth = auth;
        self
    }

    /// Sets the flag whether this node connects to remote nodes
    /// that have Onion addresses.
    /// Only outgoing connections are affected.
    /// The default is `false`.
    pub fn connect_to_onion(&mut self, b: bool) -> &mut Self {
        self.config.connect_to_onion = b;
        self
    }

    /// Sets the flag whether this node connects to remote nodes
    /// that have IP addresses.
    /// Only outgoing connections are affected.
    /// The default is `false`.
    pub fn connect_to_ip(&mut self, b: bool) -> &mut Self {
        self.config.connect_to_ip = b;
        self
    }

    /// Sets the flag whether this node can connect to this node itself.
    /// The default is `false`.
    pub fn connect_to_myself(&mut self, b: bool) -> &mut Self {
        self.config.connect_to_myself = b;
        self
    }

    /// Sets the user agent which is sent to the nodes
    /// this node connected to.
    /// The default is `"/PyBitmessage:0.6.3.2/"`.
    pub fn user_agent(&mut self, s: UserAgent) -> &mut Self {
        self.config.user_agent = s;
        self
    }

    /// Sets the set of stream numbers
    /// that this node is interested in.
    /// The default is `[1]`.
    pub fn stream_numbers(&mut self, list: StreamNumbers) -> &mut Self {
        self.config.stream_numbers = list;
        self
    }

    /// Sets the list of socket addresses of the seed nodes.
    /// The default is `[]`.
    pub fn seeds(&mut self, list: Vec<SocketAddrExt>) -> &mut Self {
        self.config.seeds = list;
        self
    }

    /// Sets the list of socket addresses of the bootstrap domain names.
    /// The default is `[]`.
    pub fn bootstraps(&mut self, list: Vec<SocketAddrNode>) -> &mut Self {
        self.config.bootstraps = list;
        self
    }

    /// Sets the maximum number of incoming connections
    /// this node accepts.
    /// The default is `160`.
    pub fn max_incoming_connected(&mut self, n: usize) -> &mut Self {
        self.config.max_incoming_connected = n;
        self
    }

    /// Sets the maximum number of incoming established connections
    /// this node accepts.
    /// The default is `128`.
    pub fn max_incoming_established(&mut self, n: usize) -> &mut Self {
        self.config.max_incoming_established = n;
        self
    }

    /// Sets the maximum number of outgoing conections
    /// this node initiates.
    /// The default is `32`.
    pub fn max_outgoing_initiated(&mut self, n: usize) -> &mut Self {
        self.config.max_outgoing_initiated = n;
        self
    }

    /// Sets the maximum number of outgoing established connections
    /// this node keeps.
    /// The default is `8`.
    pub fn max_outgoing_established(&mut self, n: usize) -> &mut Self {
        self.config.max_outgoing_established = n;
        self
    }

    /// Sets the maximum number of node addresses
    /// this node memorizes.
    /// The default is `20000`.
    pub fn max_nodes(&mut self, n: usize) -> &mut Self {
        self.config.max_nodes = n;
        self
    }

    /// Sets the list of the socket addresses of the own node.
    /// The default is `[]`.
    pub fn own_nodes(&mut self, list: Vec<SocketAddrExt>) -> &mut Self {
        self.config.own_nodes = list;
        self
    }

    /// Returns the configuration set this builder represents.
    pub fn build(&self) -> Config {
        self.config.clone()
    }
}
