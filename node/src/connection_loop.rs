// Based on: Async programming in Rust with async-std
// https://book.async.rs/tutorial/handling_disconnection.html

use std::{
    collections::{hash_map::Entry, HashMap, HashSet},
    convert::TryInto,
    fmt,
    io::Cursor,
    sync::atomic::{AtomicBool, AtomicUsize, Ordering},
    time::Duration as StdDuration,
};

use async_std::{
    io::{prelude::WriteExt, BufReader, BufWriter},
    net::{Shutdown, SocketAddr as StdSocketAddr, TcpListener, TcpStream, ToSocketAddrs},
    prelude::*,
    stream::interval,
    sync::Arc,
    task,
};
use futures::{
    channel::mpsc::{self, Receiver, Sender},
    select,
    sink::SinkExt,
    FutureExt,
};
use log::{debug, error, info, log, trace, Level};
use rand::seq::SliceRandom;

use koibumi_core::{
    address::Address,
    identity::Private as PrivateIdentity,
    io::WriteTo,
    message::{self, InvHash, NodeNonce, Pack, StreamNumber},
    net::SocketAddrExt,
    object,
    packet::{Header, Packet},
    time::Time,
};
use koibumi_socks as socks;

use crate::{
    config::Config,
    connection::{Connection, Direction},
    constant::REQUEST_EXPIRES,
    db,
    inv_manager::{manage as manage_invs, Event as InvEvent},
    manager::Event as BmEvent,
    message_handler::dispatch_message,
    net::SocketAddrNode,
    node_manager::{manage as manage_nodes, Entry as NodeEntry, Event as NodeEvent},
    pow_manager::{manage as manage_pows, Event as PowEvent},
    user_manager::{manage as manage_users, Event as UserEvent, User},
};

#[derive(Debug)]
pub struct Context {
    config: Config,
    node_nonce: NodeNonce,

    incoming_initiated: AtomicUsize,
    incoming_connected: AtomicUsize,
    incoming_established: AtomicUsize,
    outgoing_initiated: AtomicUsize,
    outgoing_connected: AtomicUsize,
    outgoing_established: AtomicUsize,

    aborted: Arc<AtomicBool>,

    pool: db::SqlitePool,

    broker_sender: Sender<Event>,
    bm_event_sender: Sender<BmEvent>,
    node_sender: Sender<NodeEvent>,
    inv_sender: Sender<InvEvent>,
    pow_sender: Sender<PowEvent>,
    user_sender: Sender<UserEvent>,
}

impl Context {
    pub fn config(&self) -> &Config {
        &self.config
    }

    pub fn node_nonce(&self) -> NodeNonce {
        self.node_nonce
    }

    pub fn initiated(&self, direction: Direction) -> &AtomicUsize {
        match direction {
            Direction::Incoming => &self.incoming_initiated,
            Direction::Outgoing => &self.outgoing_initiated,
        }
    }

    pub fn connected(&self, direction: Direction) -> &AtomicUsize {
        match direction {
            Direction::Incoming => &self.incoming_connected,
            Direction::Outgoing => &self.outgoing_connected,
        }
    }

    pub fn established(&self, direction: Direction) -> &AtomicUsize {
        match direction {
            Direction::Incoming => &self.incoming_established,
            Direction::Outgoing => &self.outgoing_established,
        }
    }

    pub fn aborted(&self) -> &Arc<AtomicBool> {
        &self.aborted
    }

    pub fn abort(&self) {
        self.aborted.store(true, Ordering::SeqCst);
    }

    pub fn pool(&self) -> &db::SqlitePool {
        &self.pool
    }

    pub fn broker_sender(&self) -> &Sender<Event> {
        &self.broker_sender
    }

    pub fn bm_event_sender(&self) -> &Sender<BmEvent> {
        &self.bm_event_sender
    }

    pub fn node_sender(&self) -> &Sender<NodeEvent> {
        &self.node_sender
    }

    pub fn inv_sender(&self) -> &Sender<InvEvent> {
        &self.inv_sender
    }

    pub fn pow_sender(&self) -> &Sender<PowEvent> {
        &self.pow_sender
    }

    pub fn user_sender(&self) -> &Sender<UserEvent> {
        &self.user_sender
    }
}

#[derive(Debug)]
pub struct ConnectionManager {
    ctx: Arc<Context>,

    broker_handle: task::JoinHandle<()>,
    server_sender: Option<Sender<Void>>,
    server_handle: Option<task::JoinHandle<()>>,
    node_handle: task::JoinHandle<()>,
    inv_handle: task::JoinHandle<()>,
    pow_handle: task::JoinHandle<()>,
    user_handle: task::JoinHandle<()>,
}

async fn accept_loop(
    addr: impl ToSocketAddrs,
    mut broker: Sender<Event>,
    mut shutdown: Receiver<Void>,
) -> Result<()> {
    let listener = TcpListener::bind(addr).await?;
    let mut incoming = listener.incoming();
    info!("Server started");
    loop {
        let stream = select! {
            stream = incoming.next().fuse() => match stream {
                Some(stream) => stream,
                None => break,
            },
            void = shutdown.next().fuse() => match void {
                Some(void) => match void {},
                None => break,
            }
        };
        let stream = stream?;
        let stream = Arc::new(stream);
        broker.send(Event::Incoming { stream }).await?;
    }
    info!("Server stopped");
    Ok(())
}

impl ConnectionManager {
    pub fn new(
        config: Config,
        pool: db::SqlitePool,
        users: Vec<User>,
        bm_event_sender: Sender<BmEvent>,
    ) -> Self {
        let (broker_sender, broker_receiver) = mpsc::channel(config.channel_buffer());
        let (node_sender, node_receiver) = mpsc::channel(config.channel_buffer());
        let (inv_sender, inv_receiver) = mpsc::channel(config.channel_buffer());
        let (pow_sender, pow_receiver) = mpsc::channel(config.channel_buffer());
        let (user_sender, user_receiver) = mpsc::channel(config.channel_buffer());

        let ctx = Arc::new(Context {
            config: config.clone(),
            node_nonce: NodeNonce::random(),

            incoming_initiated: AtomicUsize::new(0),
            incoming_connected: AtomicUsize::new(0),
            incoming_established: AtomicUsize::new(0),
            outgoing_initiated: AtomicUsize::new(0),
            outgoing_connected: AtomicUsize::new(0),
            outgoing_established: AtomicUsize::new(0),

            aborted: Arc::new(AtomicBool::new(false)),

            pool,

            broker_sender,
            bm_event_sender,
            node_sender,
            inv_sender,
            pow_sender,
            user_sender,
        });
        let broker_handle = task::spawn(broker_loop(Arc::clone(&ctx), broker_receiver));

        let server_sender;
        let server_handle;
        if let Some(server) = *ctx.config.server() {
            let (shutdown_sender, shutdown_receiver) = mpsc::channel(ctx.config.channel_buffer());
            let handle = spawn_and_log_error(
                accept_loop(server, ctx.broker_sender.clone(), shutdown_receiver),
                Level::Error,
            );
            server_sender = Some(shutdown_sender);
            server_handle = Some(handle);
        } else {
            server_sender = None;
            server_handle = None;
        }

        let node_handle = task::spawn(manage_nodes(Arc::clone(&ctx), node_receiver));
        let inv_handle = task::spawn(manage_invs(Arc::clone(&ctx), inv_receiver));
        let pow_handle = task::spawn(manage_pows(Arc::clone(&ctx), pow_receiver));
        let user_handle = task::spawn(manage_users(Arc::clone(&ctx), user_receiver));

        let mut node_sender = ctx.node_sender.clone();
        let mut user_sender = ctx.user_sender.clone();

        let cm = Self {
            ctx,

            broker_handle,
            server_sender,
            server_handle,
            node_handle,
            inv_handle,
            pow_handle,
            user_handle,
        };

        task::spawn(async move {
            let stream_number: StreamNumber = 1.into();
            let now = Time::now();
            let mut list = Vec::with_capacity(config.seeds().len());
            for addr in config.seeds() {
                let entry = NodeEntry::new(stream_number, addr.clone().into(), now);
                list.push(entry);
            }
            for addr in config.bootstraps() {
                let entry = NodeEntry::new(stream_number, addr.clone(), now);
                list.push(entry);
            }
            for addr in config.own_nodes() {
                let entry = NodeEntry::new(stream_number, addr.clone().into(), now);
                list.push(entry);
            }
            if let Err(err) = node_sender.send(NodeEvent::Add(list)).await {
                error!("{}", err);
            }

            for user in users {
                if let Err(err) = user_sender.send(UserEvent::User(user)).await {
                    error!("{}", err);
                }
            }
        });

        cm
    }

    pub fn stop(self) -> task::JoinHandle<()> {
        task::spawn(async {
            let mut broker_sender = self.ctx.broker_sender().clone();
            let mut bm_event_sender = self.ctx.bm_event_sender().clone();
            let mut node_sender = self.ctx.node_sender().clone();
            let mut inv_sender = self.ctx.inv_sender().clone();
            let mut pow_sender = self.ctx.pow_sender().clone();
            let mut user_sender = self.ctx.user_sender().clone();

            if let Some(mut shutdown_sender) = self.server_sender {
                shutdown_sender.close_channel();
            }
            broker_sender.close_channel();
            if let Some(server_handle) = self.server_handle {
                server_handle.await;
            }
            self.broker_handle.await;
            node_sender.close_channel();
            inv_sender.close_channel();
            pow_sender.close_channel();
            user_sender.close_channel();
            self.node_handle.await;
            self.inv_handle.await;
            self.pow_handle.await;
            self.user_handle.await;
            self.ctx.pool().close().await;
            if let Err(err) = bm_event_sender.send(BmEvent::Stopped).await {
                error!("{}", err);
            }
        })
    }

    pub fn ctx(&self) -> Arc<Context> {
        Arc::clone(&self.ctx)
    }

    pub async fn send(&self, header: object::Header, payload: Vec<u8>) -> Result<()> {
        let mut sender = self.ctx.pow_sender.clone();
        sender.send(PowEvent::Perform { header, payload }).await?;
        Ok(())
    }

    pub async fn add_identity(&self, id: Vec<u8>, identity: PrivateIdentity) -> Result<()> {
        let mut sender = self.ctx.user_sender.clone();
        sender.send(UserEvent::AddIdentity { id, identity }).await?;
        Ok(())
    }

    pub async fn subscribe(&self, id: Vec<u8>, address: Address) -> Result<()> {
        let mut sender = self.ctx.user_sender.clone();
        sender.send(UserEvent::Subscribe { id, address }).await?;
        Ok(())
    }
}

pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Result<T> = std::result::Result<T, Error>;

fn spawn_and_log_error<F>(fut: F, level: Level) -> task::JoinHandle<()>
where
    F: Future<Output = Result<()>> + Send + 'static,
{
    task::spawn(async move {
        if let Err(e) = fut.await {
            log!(level, "{}", e)
        }
    })
}

fn bytes_to_string(bytes: &[u8]) -> String {
    let b: Vec<u8> = bytes
        .iter()
        .map(|c| if *c < 0x20 || *c >= 0x7f { b'.' } else { *c })
        .collect();
    String::from_utf8_lossy(&b).to_string()
}

async fn inner_connection_loop(stream: Arc<TcpStream>, conn: &mut Connection) -> Result<()> {
    let reader = &mut BufReader::new(&*stream);

    match conn.direction() {
        Direction::Incoming => {}
        Direction::Outgoing => conn.write_version().await?,
    }

    loop {
        let mut bytes = [0; Header::LEN_BM];
        let n = reader.read(&mut bytes[0..1]).await?;
        if n == 0 {
            break;
        }
        assert_eq!(n, 1);
        reader.read_exact(&mut bytes[1..]).await?;
        let mut cur = Cursor::new(bytes);
        let header = Header::read_from_with_config(conn.ctx().config().core(), &mut cur)?;

        let mut payload = Vec::new();
        let mut r = reader.take(header.length().as_u32() as u64);
        r.read_to_end(&mut payload).await?;

        let packet = Packet::compose(conn.ctx().config().core(), header, payload)?;

        trace!(
            "Read command: {} length: {}",
            bytes_to_string(packet.header().command().as_ref()),
            packet.header().length()
        );

        dispatch_message(conn, packet).await?;
    }
    Ok(())
}

#[derive(Debug)]
pub struct TooManyConnectionsError;

impl fmt::Display for TooManyConnectionsError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        "too many connections".fmt(f)
    }
}

impl std::error::Error for TooManyConnectionsError {}

#[allow(clippy::too_many_arguments)]
async fn connection_loop(
    broker: Sender<Event>,
    stream: Arc<TcpStream>,
    addr: SocketAddrNode,
    direction: Direction,
    ctx: Arc<Context>,
    mut bm_event_sender: Sender<BmEvent>,
    mut node_sender: Sender<NodeEvent>,
    inv_sender: Sender<InvEvent>,
    _shutdown_sender: Sender<Void>,
) -> Result<()> {
    if direction == Direction::Incoming {
        let connected = ctx.connected(direction).load(Ordering::SeqCst);
        if connected >= ctx.config.max_incoming_connected() {
            stream.shutdown(Shutdown::Both)?;
            return Err(Error::from(TooManyConnectionsError));
        }
    }

    let mut conn = Connection::new(
        broker,
        addr.clone(),
        direction,
        Arc::clone(&ctx),
        bm_event_sender.clone(),
        node_sender.clone(),
        inv_sender,
    );

    let connected = ctx.connected(direction).fetch_add(1, Ordering::SeqCst) + 1;
    info!(
        "({:?}) Connected. Current: {} connections",
        direction, connected
    );
    conn.send_bm_event_connection_counts().await;

    let result = inner_connection_loop(stream, &mut conn).await;

    let connected = ctx.connected(direction).fetch_sub(1, Ordering::SeqCst) - 1;
    info!(
        "({:?}) Disconnected. Current: {} connections",
        direction, connected
    );
    if conn.established() {
        let established = ctx.established(direction).fetch_sub(1, Ordering::SeqCst) - 1;
        info!("({:?}) Established: {}", direction, established);
    }
    conn.send_bm_event_connection_counts().await;

    if direction == Direction::Outgoing && !conn.established() {
        if let Err(err) = node_sender
            .send(NodeEvent::ConnectionFailed(addr.clone()))
            .await
        {
            error!("{}", err);
        }
    }

    if let Err(err) = bm_event_sender.send(BmEvent::Disconnected { addr }).await {
        error!("{}", err);
    }

    result
}

async fn async_write_to<'a, W>(packet: &'a impl WriteTo, w: &'a mut W) -> async_std::io::Result<()>
where
    W: WriteExt + Unpin,
{
    let mut bytes = Vec::new();
    packet.write_to(&mut bytes).unwrap();
    w.write_all(&bytes).await
}

async fn connection_writer_loop(
    messages: &mut Receiver<Vec<Packet>>,
    stream: Arc<TcpStream>,
    shutdown: Receiver<Void>,
) -> Result<()> {
    let mut writer = BufWriter::new(&*stream);
    let mut messages = messages.fuse();
    let mut shutdown = shutdown.fuse();
    loop {
        select! {
            list = messages.next().fuse() => match list {
                Some(list) => {
                    if list.is_empty() {
                        break;
                    }
                    for packet in list {
                        trace!(
                            "Write command: {} length: {}",
                            bytes_to_string(packet.header().command().as_ref()),
                            packet.header().length()
                        );
                        async_write_to(&packet, &mut writer).await?;
                    }
                    writer.flush().await?;
                }
                None => break,
            },
            void = shutdown.next().fuse() => match void {
                Some(void) => match void {},
                None => break,
            }
        }
    }
    stream.shutdown(Shutdown::Both)?;
    Ok(())
}

#[derive(Debug)]
pub enum Void {}

#[derive(Debug)]
pub enum Event {
    Incoming {
        stream: Arc<TcpStream>,
    },
    Outgoing {
        addr: SocketAddrNode,
    },
    Write {
        addr: SocketAddrNode,
        list: Vec<Packet>,
    },
    AbortPendings,
    ObjectsNewToMe {
        addr: SocketAddrNode,
        list: Vec<InvHash>,
    },
    GetObjects {
        addr: SocketAddrNode,
        list: Vec<InvHash>,
    },
    ObjectsNewToHer {
        addr: SocketAddrNode,
        list: Vec<InvHash>,
    },
    Established {
        addr: SocketAddrNode,
    },
    Close {
        addr: SocketAddrNode,
    },
    PublishObjects(Vec<InvHash>),
}

#[allow(clippy::too_many_arguments)]
async fn connect_incoming(
    ctx: Arc<Context>,
    addr: SocketAddrNode,
    broker_sender: Sender<Event>,
    bm_event_sender: Sender<BmEvent>,
    node_sender: Sender<NodeEvent>,
    inv_sender: Sender<InvEvent>,
    shutdown_sender: Sender<Void>,
    stream: Arc<TcpStream>,
) -> Result<()> {
    spawn_and_log_error(
        connection_loop(
            broker_sender,
            stream,
            addr,
            Direction::Incoming,
            ctx,
            bm_event_sender,
            node_sender,
            inv_sender,
            shutdown_sender,
        ),
        Level::Debug,
    );
    Ok(())
}

#[derive(Debug)]
pub enum ConnectOutgoingError {
    Error(Error),
    Aborted,
}

impl fmt::Display for ConnectOutgoingError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Error(err) => err.fmt(f),
            Self::Aborted => "aborted".fmt(f),
        }
    }
}

impl std::error::Error for ConnectOutgoingError {}

impl From<socks::ConnectError> for ConnectOutgoingError {
    fn from(err: socks::ConnectError) -> Self {
        ConnectOutgoingError::Error(Error::from(err))
    }
}

#[allow(clippy::too_many_arguments)]
async fn connect_outgoing(
    ctx: Arc<Context>,
    addr: SocketAddrNode,
    broker_sender: Sender<Event>,
    bm_event_sender: Sender<BmEvent>,
    node_sender: Sender<NodeEvent>,
    inv_sender: Sender<InvEvent>,
    shutdown_sender: Sender<Void>,
    mut abort_receiver: Receiver<Void>,
) -> std::result::Result<Arc<TcpStream>, ConnectOutgoingError> {
    let mut stream;
    if let Some(server) = ctx.config.socks() {
        stream = match TcpStream::connect(server).await {
            Ok(stream) => stream,
            Err(err) => return Err(ConnectOutgoingError::Error(Error::from(err))),
        };
        if let Some(auth) = ctx.config.socks_auth() {
            select! {
                dest = socks::connect_with_password(&mut stream,
                    auth.username(), auth.password(),
                    addr.clone().into()).fuse() => {
                    dest?
                },
                _ = abort_receiver.next().fuse() => {
                    return Err(ConnectOutgoingError::Aborted)
                },
            };
        } else {
            select! {
                dest = socks::connect(&mut stream, addr.clone().into()).fuse() => {
                    dest?
                },
                _ = abort_receiver.next().fuse() => {
                    return Err(ConnectOutgoingError::Aborted)
                },
            };
        }
    } else {
        match &addr {
            SocketAddrNode::AddrExt(addr) => {
                let server: StdSocketAddr = match addr.clone().try_into() {
                    Ok(addr) => addr,
                    Err(err) => return Err(ConnectOutgoingError::Error(Error::from(err))),
                };
                stream = select! {
                    stream = TcpStream::connect(server).fuse() => {
                        match stream {
                            Ok(stream) => stream,
                            Err(err) => return Err(ConnectOutgoingError::Error(Error::from(err))),
                        }
                    },
                    _ = abort_receiver.next().fuse() => {
                        return Err(ConnectOutgoingError::Aborted)
                    },
                };
            }
            SocketAddrNode::Domain(domain) => {
                stream = select! {
                    stream = TcpStream::connect(domain.to_string()).fuse() => {
                        match stream {
                            Ok(stream) => stream,
                            Err(err) => return Err(ConnectOutgoingError::Error(Error::from(err))),
                        }
                    },
                    _ = abort_receiver.next().fuse() => {
                        return Err(ConnectOutgoingError::Aborted)
                    },
                };
            }
        }
    }
    let stream = Arc::new(stream);
    let s = Arc::clone(&stream);
    spawn_and_log_error(
        connection_loop(
            broker_sender.clone(),
            s,
            addr,
            Direction::Outgoing,
            ctx,
            bm_event_sender,
            node_sender,
            inv_sender,
            shutdown_sender,
        ),
        Level::Debug,
    );
    Ok(stream)
}

async fn send_bm_event_connection_counts(ctx: &Context, bm_event_sender: &mut Sender<BmEvent>) {
    if let Err(err) = bm_event_sender
        .send(BmEvent::ConnectionCounts {
            incoming_initiated: ctx.initiated(Direction::Incoming).load(Ordering::SeqCst),
            incoming_connected: ctx.connected(Direction::Incoming).load(Ordering::SeqCst),
            incoming_established: ctx.established(Direction::Incoming).load(Ordering::SeqCst),
            outgoing_initiated: ctx.initiated(Direction::Outgoing).load(Ordering::SeqCst),
            outgoing_connected: ctx.connected(Direction::Outgoing).load(Ordering::SeqCst),
            outgoing_established: ctx.established(Direction::Outgoing).load(Ordering::SeqCst),
        })
        .await
    {
        error!("{}", err);
    }
}

async fn broker_loop(ctx: Arc<Context>, events: Receiver<Event>) {
    let mut broker_sender = ctx.broker_sender.clone();
    let mut bm_event_sender = ctx.bm_event_sender.clone();
    let mut node_sender = ctx.node_sender.clone();
    let mut inv_sender = ctx.inv_sender.clone();

    let (disconnect_sender, mut disconnect_receiver) =
        mpsc::channel::<(SocketAddrNode, Receiver<Vec<Packet>>)>(ctx.config.channel_buffer());
    let mut peers: HashMap<SocketAddrNode, Sender<Vec<Packet>>> = HashMap::new();
    let mut abort_senders: HashMap<SocketAddrNode, Sender<Void>> = HashMap::new();
    let mut objects_new_to_me: HashMap<SocketAddrNode, Vec<InvHash>> = HashMap::new();
    let mut pending_uploads: HashMap<SocketAddrNode, HashSet<InvHash>> = HashMap::new();
    let mut objects_new_to_her: HashMap<SocketAddrNode, HashMap<InvHash, Time>> = HashMap::new();
    let mut established: HashSet<SocketAddrNode> = HashSet::new();

    let mut events = events.fuse();
    let mut transfer_interval = interval(StdDuration::from_secs(1));
    let mut clean_objects_new_to_her_interval = interval(StdDuration::from_secs(300));
    let mut ping_interval = interval(StdDuration::from_secs(120));
    loop {
        let event = select! {
            event = events.next().fuse() => match event {
                None => break,
                Some(event) => event,
            },
            disconnect = disconnect_receiver.next().fuse() => {
                let (addr, _pending_messages) = disconnect.unwrap();
                assert!(peers.remove(&addr).is_some());
                if abort_senders.remove(&addr).is_some() {
                    ctx.initiated(Direction::Outgoing).fetch_sub(1, Ordering::SeqCst);
                } else {
                    ctx.initiated(Direction::Incoming).fetch_sub(1, Ordering::SeqCst);
                }
                objects_new_to_me.remove(&addr);
                pending_uploads.remove(&addr);
                objects_new_to_her.remove(&addr);
                established.remove(&addr);
                if let Err(err) = node_sender.send(NodeEvent::Disconnected(addr.clone())).await {
                    error!("{}", err);
                }
                send_bm_event_connection_counts(&ctx, &mut bm_event_sender).await;
                continue;
            },
            tick = transfer_interval.next().fuse() => match tick {
                Some(_) => {
                    for (addr, vec) in &mut objects_new_to_me {
                        if !established.contains(&addr) {
                            continue;
                        }
                        if vec.is_empty() {
                            continue;
                        }
                        vec.shuffle(&mut rand::thread_rng());
                        const MAX_COUNT: usize = 1000;
                        let count = {
                            let mut iter = vec.chunks(MAX_COUNT);
                            if let Some(chunk) = iter.next() {
                                let getdata = message::Getdata::new(chunk.to_vec()).unwrap();
                                let packet = getdata.pack(ctx.config().core()).unwrap();
                                if let Err(err) = broker_sender.send(
                                    Event::Write { addr: addr.clone(), list: vec![packet] }).await {
                                    error!("{}", err);
                                }
                                chunk.len()
                            } else {
                                0
                            }
                        };
                        vec.drain(0..count);
                    }

                    for (addr, set) in &mut pending_uploads {
                        if !established.contains(&addr) {
                            continue;
                        }
                        if set.is_empty() {
                            continue;
                        }
                        let mut vec: Vec<InvHash> = set.iter().cloned().collect();
                        vec.shuffle(&mut rand::thread_rng());
                        const MAX_COUNT: usize = 1000;
                        let count = {
                            let mut iter = vec.chunks(MAX_COUNT);
                            if let Some(chunk) = iter.next() {
                                if let Err(err) = inv_sender.send(
                                    InvEvent::SendObjects { addr: addr.clone(), list: chunk.to_vec() }
                                ).await {
                                    error!("{}", err);
                                }
                                chunk.len()
                            } else {
                                0
                            }
                        };
                        for hash in vec[0..count].iter() {
                            set.remove(&hash);
                        }
                    }

                    for (addr, map) in &mut objects_new_to_her {
                        if !established.contains(&addr) {
                            continue;
                        }
                        if map.is_empty() {
                            continue;
                        }
                        let mut vec: Vec<InvHash> = map.keys().cloned().collect();
                        vec.shuffle(&mut rand::thread_rng());
                        const MAX_COUNT: usize = 1000;
                        let mut iter = vec.chunks(MAX_COUNT);
                        if let Some(chunk) = iter.next() {
                            let inv = message::Inv::new(chunk.to_vec()).unwrap();
                            let packet = inv.pack(ctx.config().core()).unwrap();
                            if let Err(err) = broker_sender
                                .send(Event::Write {
                                    addr: addr.clone(),
                                    list: vec![packet],
                                })
                                .await
                            {
                                error!("{}", err);
                            }

                            for hash in chunk {
                                map.remove(&hash);
                            }
                        }
                    }
                    continue;
                },
                None => break,
            },
            tick = clean_objects_new_to_her_interval.next().fuse() => match tick {
                Some(_) => {
                    let now = Time::now();
                    for map in objects_new_to_her.values_mut() {
                        map.retain(|_hash, time| {
                            match time.checked_add(REQUEST_EXPIRES) {
                                Some(target) => now < target,
                                None => false,
                            }
                        });
                    }
                    continue;
                },
                None => break,
            },
            tick = ping_interval.next().fuse() => match tick {
                Some(_) => {
                    let packet = message::Ping::new().pack(ctx.config().core()).unwrap();
                    for addr in &established {
                        if let Err(err) = broker_sender.send(
                            Event::Write { addr: addr.clone(), list: vec![packet.clone()]}
                        ).await {
                            error!("{}", err);
                        }
                    }
                    continue;
                },
                None => break,
            },
        };
        match event {
            Event::Incoming { stream } => {
                if let Ok(addr) = stream.peer_addr() {
                    let addr: SocketAddrExt = addr.into();
                    let addr: SocketAddrNode = addr.into();
                    match peers.entry(addr.clone()) {
                        Entry::Occupied(..) => (),
                        Entry::Vacant(entry) => {
                            let ctx = Arc::clone(&ctx);
                            let addr = addr.clone();
                            let broker_sender = broker_sender.clone();
                            let mut bm_event_sender = bm_event_sender.clone();
                            let node_sender = node_sender.clone();
                            let inv_sender = inv_sender.clone();
                            let mut disconnect_sender = disconnect_sender.clone();
                            let (client_sender, mut client_receiver) =
                                mpsc::channel(ctx.config.channel_buffer());
                            entry.insert(client_sender);
                            objects_new_to_me.insert(addr.clone(), Vec::new());
                            pending_uploads.insert(addr.clone(), HashSet::new());
                            objects_new_to_her.insert(addr.clone(), HashMap::new());
                            ctx.initiated(Direction::Incoming)
                                .fetch_add(1, Ordering::SeqCst);
                            send_bm_event_connection_counts(&ctx, &mut bm_event_sender).await;
                            spawn_and_log_error(
                                async move {
                                    let (shutdown_sender, shutdown_receiver) =
                                        mpsc::channel::<Void>(ctx.config.channel_buffer());
                                    let mut e: Option<Error> = None;
                                    match connect_incoming(
                                        ctx,
                                        addr.clone(),
                                        broker_sender,
                                        bm_event_sender,
                                        node_sender,
                                        inv_sender,
                                        shutdown_sender,
                                        Arc::clone(&stream),
                                    )
                                    .await
                                    {
                                        Ok(_) => {
                                            if let Err(err) = connection_writer_loop(
                                                &mut client_receiver,
                                                stream,
                                                shutdown_receiver,
                                            )
                                            .await
                                            {
                                                e = Some(err)
                                            }
                                        }
                                        Err(err) => e = Some(err),
                                    }
                                    disconnect_sender
                                        .send((addr, client_receiver))
                                        .await
                                        .unwrap();
                                    if let Some(err) = e {
                                        return Err(err);
                                    }
                                    Ok(())
                                },
                                Level::Debug,
                            );
                        }
                    }
                }
            }
            Event::Outgoing { addr } => {
                if !ctx.config.is_connectable_to(&addr) {
                    continue;
                }
                match peers.entry(addr.clone()) {
                    Entry::Occupied(..) => (),
                    Entry::Vacant(entry) => {
                        let ctx = Arc::clone(&ctx);
                        let addr = addr.clone();
                        let broker_sender = broker_sender.clone();
                        let mut bm_event_sender = bm_event_sender.clone();
                        let mut node_sender = node_sender.clone();
                        let inv_sender = inv_sender.clone();
                        let mut disconnect_sender = disconnect_sender.clone();
                        let (client_sender, mut client_receiver) =
                            mpsc::channel(ctx.config.channel_buffer());
                        entry.insert(client_sender);
                        let (abort_sender, abort_receiver) =
                            mpsc::channel::<Void>(ctx.config.channel_buffer());
                        abort_senders.insert(addr.clone(), abort_sender);
                        objects_new_to_me.insert(addr.clone(), Vec::new());
                        pending_uploads.insert(addr.clone(), HashSet::new());
                        objects_new_to_her.insert(addr.clone(), HashMap::new());
                        ctx.initiated(Direction::Outgoing)
                            .fetch_add(1, Ordering::SeqCst);
                        send_bm_event_connection_counts(&ctx, &mut bm_event_sender).await;
                        spawn_and_log_error(
                            async move {
                                let (shutdown_sender, shutdown_receiver) =
                                    mpsc::channel::<Void>(ctx.config.channel_buffer());
                                let mut e: Option<Error> = None;
                                match connect_outgoing(
                                    ctx,
                                    addr.clone(),
                                    broker_sender,
                                    bm_event_sender,
                                    node_sender.clone(),
                                    inv_sender,
                                    shutdown_sender,
                                    abort_receiver,
                                )
                                .await
                                {
                                    Ok(stream) => {
                                        if let Err(err) = connection_writer_loop(
                                            &mut client_receiver,
                                            stream,
                                            shutdown_receiver,
                                        )
                                        .await
                                        {
                                            e = Some(err)
                                        }
                                    }
                                    Err(err) => match err {
                                        ConnectOutgoingError::Error(err) => {
                                            if let Err(err) = node_sender
                                                .send(NodeEvent::ConnectionFailed(addr.clone()))
                                                .await
                                            {
                                                error!("{}", err);
                                            }
                                            e = Some(err)
                                        }
                                        ConnectOutgoingError::Aborted => e = Some(Error::from(err)),
                                    },
                                }
                                disconnect_sender
                                    .send((addr, client_receiver))
                                    .await
                                    .unwrap();
                                if let Some(err) = e {
                                    return Err(err);
                                }
                                Ok(())
                            },
                            Level::Debug,
                        );
                    }
                }
            }
            Event::Write { addr, list } => {
                if let Some(peer) = peers.get_mut(&addr) {
                    if let Err(err) = peer.send(list).await {
                        error!("{}", err);
                    }
                }
            }
            Event::AbortPendings => {
                for sender in abort_senders.values_mut() {
                    if let Err(err) = sender.close().await {
                        error!("{}", err);
                    }
                }
            }
            Event::ObjectsNewToMe { addr, mut list } => {
                if let Some(vec) = objects_new_to_me.get_mut(&addr) {
                    vec.append(&mut list);
                }
            }
            Event::GetObjects { addr, list } => {
                if let Some(set) = pending_uploads.get_mut(&addr) {
                    for hash in list {
                        set.insert(hash);
                    }
                }
            }
            Event::ObjectsNewToHer { addr, list } => {
                let now = Time::now();
                for (a, map) in &mut objects_new_to_her {
                    if *a == addr {
                        continue;
                    }
                    for hash in &list {
                        map.insert(hash.clone(), now);
                    }
                }
            }
            Event::Established { addr } => {
                established.insert(addr);
            }
            Event::Close { addr } => {
                if let Some(peer) = peers.get_mut(&addr) {
                    if let Err(err) = peer.send(Vec::with_capacity(0)).await {
                        error!("{}", err);
                    }
                }
            }
            Event::PublishObjects(list) => {
                let now = Time::now();
                for map in objects_new_to_her.values_mut() {
                    for hash in &list {
                        map.insert(hash.clone(), now);
                    }
                }
            }
        }
    }

    debug!("abort_senders: {}", abort_senders.len());
    drop(abort_senders);
    ctx.initiated(Direction::Outgoing)
        .store(0, Ordering::SeqCst);
    debug!("peers: {}", peers.len());
    drop(peers);
    drop(objects_new_to_me);
    drop(pending_uploads);
    drop(objects_new_to_her);
    drop(established);
    drop(disconnect_sender);
    ctx.initiated(Direction::Incoming)
        .store(0, Ordering::SeqCst);
    while let Some((_name, _pending_messages)) = disconnect_receiver.next().await {}
}
