use koibumi_core::time::Duration;

pub const MAX_TIME_OFFSET: Duration = Duration::new(3600);

pub const OBJECT_PAST_LIFETIME: Duration = Duration::new(60 * 60);
pub const OBJECT_FUTURE_LIFETIME: Duration = Duration::new(28 * 24 * 60 * 60 + 3 * 60 * 60);
pub const REQUEST_EXPIRES: Duration = Duration::new(3600);
