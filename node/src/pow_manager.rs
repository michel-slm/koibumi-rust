use std::{
    collections::HashMap,
    io::Cursor,
    sync::atomic::{AtomicBool, Ordering},
    time::Duration as StdDuration,
};

use async_std::{sync::Arc, task};
use futures::{
    channel::mpsc::{self, Receiver},
    sink::SinkExt,
    stream::StreamExt,
};
use log::{error, info};

use koibumi_core::{
    hash::{sha512, Hash512},
    io::{LenBm, ReadFrom, WriteTo},
    message, object, pow,
    time::Time,
};

use crate::{connection_loop::Context, inv_manager::Event as InvEvent};

#[derive(Debug)]
pub enum Event {
    Perform {
        header: object::Header,
        payload: Vec<u8>,
    },
    Done(message::Object),
}

fn initial_hash(payload: impl AsRef<[u8]>) -> Hash512 {
    sha512(payload)
}

async fn perform_pow(
    ctx: Arc<Context>,
    payload: impl AsRef<[u8]>,
    expires_time: Time,
) -> Result<pow::Nonce, pow::PerformError> {
    let cpus = num_cpus::get_physical().max(1);

    let cancel = Arc::new(AtomicBool::new(false));
    let (mut sender, mut receiver) =
        mpsc::channel::<Result<pow::Nonce, pow::PerformError>>(cpus + 1);
    for i in 0..cpus {
        let mut sender = sender.clone();
        let ctx = Arc::clone(&ctx);
        let len = payload.as_ref().len();
        let initial_hash = pow::initial_hash(payload.as_ref());
        let nonce_trials_per_byte = ctx.config().core().nonce_trials_per_byte();
        let payload_length_extra_bytes = ctx.config().core().payload_length_extra_bytes();
        let cancel = Arc::clone(&cancel);
        task::spawn_blocking(move || {
            let target = pow::target(
                ctx.config().core(),
                len,
                expires_time,
                Time::now(),
                nonce_trials_per_byte,
                payload_length_extra_bytes,
            );
            let result = pow::perform(
                initial_hash,
                target,
                (i as u64).into(),
                cpus,
                Arc::clone(&cancel),
            );
            task::block_on(async {
                if let Err(err) = sender.send(result).await {
                    error!("{}", err);
                }
            });
            cancel.store(true, Ordering::SeqCst);
        });
    }
    {
        let aborted = Arc::clone(ctx.aborted());
        let cancel = Arc::clone(&cancel);
        task::spawn(async move {
            loop {
                if aborted.load(Ordering::SeqCst) {
                    break;
                }
                if cancel.load(Ordering::SeqCst) {
                    break;
                }
                task::sleep(StdDuration::from_secs(1)).await;
            }
            cancel.store(true, Ordering::SeqCst);
            if let Err(err) = sender.send(Err(pow::PerformError::Canceled)).await {
                error!("{}", err);
            }
        });
    }

    match receiver.next().await {
        Some(result) => result,
        None => Err(pow::PerformError::Canceled),
    }
}

pub async fn manage(ctx: Arc<Context>, mut receiver: Receiver<Event>) {
    let pool = ctx.pool().clone();
    let mut sender = ctx.pow_sender().clone();
    let mut inv_sender = ctx.inv_sender().clone();

    if let Err(err) = sqlx::query(
        "CREATE TABLE IF NOT EXISTS pendings (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            hash BLOB NOT NULL,
            header BLOB NOT NULL,
            payload BLOB NOT NULL
        )",
    )
    .execute(pool.write())
    .await
    {
        error!("{}", err);
    }

    if let Ok(list) = sqlx::query_as::<sqlx::Sqlite, (Vec<u8>, Vec<u8>)>(
        "SELECT header, payload FROM pendings ORDER BY id",
    )
    .fetch_all(pool.read())
    .await
    {
        for elem in list {
            let header = {
                let mut r = Cursor::new(elem.0);
                object::Header::read_from(&mut r)
            };
            if let Err(err) = header {
                error!("{}", err);
                continue;
            }
            if let Err(err) = sender
                .send(Event::Perform {
                    header: header.unwrap(),
                    payload: elem.1,
                })
                .await
            {
                error!("{}", err);
            }
        }
    }

    let mut handles: HashMap<Hash512, task::JoinHandle<()>> = HashMap::new();

    while let Some(event) = receiver.next().await {
        match event {
            Event::Perform { header, payload } => {
                info!("PoW start");

                let mut bytes = Vec::with_capacity(header.len_bm() as usize + payload.len());
                header.write_to(&mut bytes).unwrap();
                payload.write_to(&mut bytes).unwrap();
                let hash = initial_hash(&bytes);

                let mut header_bytes = Vec::with_capacity(header.len_bm());
                header.write_to(&mut header_bytes).unwrap();

                if let Ok(list) = sqlx::query_as::<sqlx::Sqlite, (Vec<u8>, Vec<u8>)>(
                    "SELECT header, payload FROM pendings WHERE hash=?1",
                )
                .bind(hash.as_ref())
                .fetch_all(pool.read())
                .await
                {
                    if list.is_empty() {
                        if let Err(err) = sqlx::query(
                            "INSERT INTO pendings (
                                    hash, header, payload
                                ) VALUES (?1, ?2, ?3)",
                        )
                        .bind(hash.as_ref())
                        .bind(header_bytes)
                        .bind(&payload)
                        .execute(pool.write())
                        .await
                        {
                            error!("{}", err);
                        }
                    }
                }

                let ctx = Arc::clone(&ctx);
                let expires = header.expires_time();
                let mut sender = sender.clone();
                let handle = task::spawn(async move {
                    let nonce = perform_pow(ctx, &bytes, expires).await;
                    if let Err(err) = nonce {
                        error!("{}", err);
                        return;
                    }
                    let nonce = nonce.unwrap();

                    match message::Object::new(nonce, header, payload) {
                        Ok(message) => {
                            if let Err(err) = sender.send(Event::Done(message)).await {
                                error!("{}", err);
                            }
                        }
                        Err(err) => {
                            error!("{}", err);
                            // TODO cancel
                        }
                    }
                });
                handles.insert(hash, handle);
            }
            Event::Done(message) => {
                let mut bytes = Vec::with_capacity(
                    message.header().len_bm() as usize + message.object_payload().len(),
                );
                message.header().write_to(&mut bytes).unwrap();
                message.object_payload().write_to(&mut bytes).unwrap();
                let hash = initial_hash(bytes);

                if let Err(err) = inv_sender.send(InvEvent::Insert(message)).await {
                    error!("{}", err);
                } else if let Err(err) = sqlx::query("DELETE FROM pendings WHERE hash=?1")
                    .bind(hash.as_ref())
                    .execute(pool.write())
                    .await
                {
                    error!("{}", err);
                }

                handles.remove(&hash);

                info!("PoW done");
            }
        }
    }

    for handle in handles.values_mut() {
        handle.await;
    }
}
