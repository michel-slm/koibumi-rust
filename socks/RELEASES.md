# Version 0.0.5 (2021-01-)

* Added `deny(unsafe_code)` directive.

# Version 0.0.4 (2020-09-09)

* Updated libraries.

# Version 0.0.3 (2020-09-07)

* Split away core part into koibumi-socks-core.

# Version 0.0.2 (2020-07-28)

* Now the crate depends on `futures` directly instead of `async-std`.

# Version 0.0.1 (2020-05-26)

* Workaround for recent Tor's SOCKS5 server

# Version 0.0 (2020-05-23)

* Features
    * Connect to the specified destination via a SOCKS5 proxy

Limitations:

* No authentications are supported.
