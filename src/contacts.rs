use async_std::task;
use copypasta::ClipboardProvider;
use futures::{channel::mpsc::Sender, sink::SinkExt};
use iced::{button, scrollable, Button, Column, Command, Element, Radio, Row, Scrollable, Text};
use log::error;

use koibumi_box::Contact;
use koibumi_common::boxes::{Boxes, DEFAULT_USER_ID};
use koibumi_node::Command as NodeCommand;

use crate::{config::Config as GuiConfig, gui};

#[derive(Clone, Debug)]
pub enum Message {
    ContactSelected(usize),
    CopyPressed,
    SubscribePressed,
}

#[derive(Clone, Debug, Default)]
pub(crate) struct Tab {
    scroll: scrollable::State,
    copy_button: button::State,
    subscribe_button: button::State,
}

impl Tab {
    pub(crate) fn update(
        &mut self,
        message: Message,
        boxes: &mut Option<Boxes>,
        command_sender: &mut Sender<NodeCommand>,
    ) -> Command<gui::Message> {
        if boxes.is_none() {
            return Command::none();
        }
        let boxes = boxes.as_mut().unwrap();
        match message {
            Message::ContactSelected(index) => {
                boxes.set_selected_contact_index(Some(index));
                Command::none()
            }

            Message::CopyPressed => {
                if boxes.selected_contact_index().is_none() {
                    return Command::none();
                }
                let index = boxes.selected_contact_index().unwrap();
                let address = boxes.user().contacts()[index].address();
                let ctx = copypasta::ClipboardContext::new();
                if let Err(err) = ctx {
                    error!("{}", err);
                    return Command::none();
                }
                let mut ctx = ctx.unwrap();
                if let Err(err) = ctx.set_contents(address.to_string()) {
                    error!("{}", err);
                }
                Command::none()
            }
            Message::SubscribePressed => {
                if boxes.selected_contact_index().is_none() {
                    return Command::none();
                }
                let index = boxes.selected_contact_index().unwrap();
                let address = boxes.user().contacts()[index].address().clone();
                if boxes.user().subscriptions().contains(&address) {
                    return Command::none();
                }
                if let Err(err) =
                    task::block_on(boxes.manager().subscribe(DEFAULT_USER_ID, &address))
                {
                    error!("{}", err);
                    return Command::none();
                }
                if let Err(err) = task::block_on(command_sender.send(NodeCommand::Subscribe {
                    id: DEFAULT_USER_ID.to_vec(),
                    address: address.clone(),
                })) {
                    error!("{}", err);
                    return Command::none();
                }
                boxes.user_mut().subscriptions_mut().push(address);
                Command::none()
            }
        }
    }

    pub(crate) fn view(
        &mut self,
        config: &GuiConfig,
        contacts: &[Contact],
        selected_index: Option<usize>,
        boxes: &Option<Boxes>,
    ) -> Element<gui::Message> {
        let text_size = config.text_size();

        if boxes.is_none() {
            return Column::new()
                .push(Text::new("inbox/outbox database error").size(text_size))
                .into();
        }
        let boxes = boxes.as_ref().unwrap();

        let mut list = Scrollable::new(&mut self.scroll)
            .max_height(text_size as u32 * 16)
            .spacing(text_size / 4);
        let mut i = 0;
        for contact in contacts {
            let alias = boxes.user().rich_alias(&contact.address().to_string());
            let row = Row::new().push(Radio::new(i, alias, selected_index, |a| {
                gui::Message::ContactsMessage(Message::ContactSelected(a))
            }));
            list = list.push(row);
            i += 1;
        }

        let buttons = Row::new()
            .spacing(text_size / 4)
            .push(
                Button::new(
                    &mut self.copy_button,
                    Text::new("Copy to clipboard").size(text_size),
                )
                .on_press(gui::Message::ContactsMessage(Message::CopyPressed)),
            )
            .push(
                Button::new(
                    &mut self.subscribe_button,
                    Text::new("Subscribe").size(text_size),
                )
                .on_press(gui::Message::ContactsMessage(Message::SubscribePressed)),
            );

        Column::new()
            .spacing(text_size / 4)
            .push(list)
            .push(buttons)
            .into()
    }
}
