use std::sync::{atomic::AtomicBool, Arc};

use async_std::task;
use copypasta::ClipboardProvider;
use futures::{channel::mpsc::Sender, sink::SinkExt};
use iced::{
    button, scrollable, text_input, Button, Column, Command, Element, Radio, Row, Scrollable, Text,
    TextInput,
};
use log::error;

use koibumi_box::Contact;
use koibumi_common::boxes::{Boxes, DEFAULT_USER_ID};
use koibumi_core::identity::Private as PrivateIdentity;
use koibumi_node::Command as NodeCommand;

use crate::{config::Config as GuiConfig, gui, log::Logger};

#[derive(Clone, Debug)]
pub enum Message {
    IdentitySelected(usize),
    CopyPressed,
    SubscribePressed,
    GenerateRandomPressed,
    DeterministicPasswordChanged(String),
    GenerateDeterministicPressed,
    ChanPasswordChanged(String),
    GenerateChanPressed,
}

#[derive(Clone, Debug, Default)]
pub(crate) struct Tab {
    scroll: scrollable::State,
    copy_button: button::State,
    subscribe_button: button::State,
    generate_random_button: button::State,
    deterministic_password: text_input::State,
    deterministic_password_value: String,
    generate_deterministic_button: button::State,
    chan_password: text_input::State,
    chan_password_value: String,
    generate_chan_button: button::State,
}

impl Tab {
    pub(crate) fn update(
        &mut self,
        message: Message,
        boxes: &mut Option<Boxes>,
        command_sender: &mut Sender<NodeCommand>,
        logger: &mut Logger,
    ) -> Command<gui::Message> {
        if boxes.is_none() {
            return Command::none();
        }
        let boxes = boxes.as_mut().unwrap();
        match message {
            Message::IdentitySelected(index) => {
                boxes.set_selected_identity_index(Some(index));
                Command::none()
            }

            Message::CopyPressed => {
                if boxes.selected_identity_index().is_none() {
                    return Command::none();
                }
                let index = boxes.selected_identity_index().unwrap();
                let address = boxes.user().private_identities()[index].address();
                let ctx = copypasta::ClipboardContext::new();
                if let Err(err) = ctx {
                    error!("{}", err);
                    return Command::none();
                }
                let mut ctx = ctx.unwrap();
                if let Err(err) = ctx.set_contents(address.to_string()) {
                    error!("{}", err);
                }
                Command::none()
            }
            Message::SubscribePressed => {
                if boxes.selected_identity_index().is_none() {
                    return Command::none();
                }
                let index = boxes.selected_identity_index().unwrap();
                let address = boxes.user().private_identities()[index].address();
                if boxes.user().subscriptions().contains(&address) {
                    return Command::none();
                }
                if let Err(err) =
                    task::block_on(boxes.manager().subscribe(DEFAULT_USER_ID, &address))
                {
                    error!("{}", err);
                    return Command::none();
                }
                if let Err(err) = task::block_on(command_sender.send(NodeCommand::Subscribe {
                    id: DEFAULT_USER_ID.to_vec(),
                    address: address.clone(),
                })) {
                    error!("{}", err);
                    return Command::none();
                }
                boxes.user_mut().subscriptions_mut().push(address);
                Command::none()
            }

            Message::GenerateRandomPressed => {
                let cancel = Arc::new(AtomicBool::new(false));
                let identity = PrivateIdentity::random_builder().build(cancel);
                if let Err(err) = identity {
                    error!("{}", err);
                    return Command::none();
                }
                let identity = identity.unwrap();
                if let Err(err) = task::block_on(
                    boxes
                        .manager()
                        .add_private_identity(DEFAULT_USER_ID, identity.clone()),
                ) {
                    error!("{}", err);
                    return Command::none();
                }
                if let Err(err) = task::block_on(command_sender.send(NodeCommand::AddIdentity {
                    id: DEFAULT_USER_ID.to_vec(),
                    identity: identity.clone(),
                })) {
                    error!("{}", err);
                    return Command::none();
                }
                boxes
                    .user_mut()
                    .private_identities_mut()
                    .insert(0, identity);
                boxes.set_selected_identity_index(None);
                logger.info("Random address generated");
                Command::none()
            }

            Message::DeterministicPasswordChanged(password) => {
                self.deterministic_password_value = password;
                Command::none()
            }
            Message::GenerateDeterministicPressed => {
                let cancel = Arc::new(AtomicBool::new(false));
                let identities = PrivateIdentity::deterministic_builder(
                    self.deterministic_password_value.as_bytes().to_vec(),
                )
                .build(1, cancel);
                if let Err(err) = identities {
                    error!("{}", err);
                    return Command::none();
                }
                let identities = identities.unwrap();
                let identity = identities[0].clone();
                if let Err(err) = task::block_on(
                    boxes
                        .manager()
                        .add_private_identity(DEFAULT_USER_ID, identity.clone()),
                ) {
                    error!("{}", err);
                    return Command::none();
                }
                if let Err(err) = task::block_on(command_sender.send(NodeCommand::AddIdentity {
                    id: DEFAULT_USER_ID.to_vec(),
                    identity: identity.clone(),
                })) {
                    error!("{}", err);
                    return Command::none();
                }
                boxes
                    .user_mut()
                    .private_identities_mut()
                    .insert(0, identity);
                boxes.set_selected_identity_index(None);
                self.deterministic_password_value = String::new();
                logger.info("Deterministic address generated");
                Command::none()
            }

            Message::ChanPasswordChanged(password) => {
                self.chan_password_value = password;
                Command::none()
            }
            Message::GenerateChanPressed => {
                let cancel = Arc::new(AtomicBool::new(false));
                let identity =
                    PrivateIdentity::chan_builder(self.chan_password_value.as_bytes().to_vec())
                        .build(cancel);
                if let Err(err) = identity {
                    error!("{}", err);
                    return Command::none();
                }
                let identity = identity.unwrap();
                if let Err(err) = task::block_on(
                    boxes
                        .manager()
                        .add_private_identity(DEFAULT_USER_ID, identity.clone()),
                ) {
                    error!("{}", err);
                    return Command::none();
                }
                if let Err(err) = task::block_on(command_sender.send(NodeCommand::AddIdentity {
                    id: DEFAULT_USER_ID.to_vec(),
                    identity: identity.clone(),
                })) {
                    error!("{}", err);
                    return Command::none();
                }
                let address = identity.address();
                boxes
                    .user_mut()
                    .private_identities_mut()
                    .insert(0, identity);
                boxes.set_selected_identity_index(None);

                let contact = Contact::new(address.clone());
                if let Err(err) =
                    task::block_on(boxes.manager().add_contact(DEFAULT_USER_ID, &contact))
                {
                    error!("{}", err);
                    return Command::none();
                }
                boxes.user_mut().contacts_mut().push(contact);

                let alias = format!("[chan] {}", self.chan_password_value);
                if let Err(err) =
                    task::block_on(boxes.manager().add_alias(DEFAULT_USER_ID, &address, &alias))
                {
                    error!("{}", err);
                    return Command::none();
                }
                boxes
                    .user_mut()
                    .aliases_mut()
                    .insert(address.to_string(), alias);

                self.chan_password_value = String::new();
                logger.info("chan address generated");
                Command::none()
            }
        }
    }

    pub(crate) fn view(
        &mut self,
        config: &GuiConfig,
        identities: &[PrivateIdentity],
        selected_index: Option<usize>,
        boxes: &Option<Boxes>,
    ) -> Element<gui::Message> {
        let text_size = config.text_size();

        if boxes.is_none() {
            return Column::new()
                .push(Text::new("inbox/outbox database error").size(text_size))
                .into();
        }
        let boxes = boxes.as_ref().unwrap();

        let mut list = Scrollable::new(&mut self.scroll)
            .max_height(text_size as u32 * 16)
            .spacing(text_size / 4);
        let mut i = 0;
        for identity in identities {
            let alias = boxes.user().rich_alias(&identity.to_string());
            let row = Row::new().push(Radio::new(i, alias, selected_index, |a| {
                gui::Message::IdentitiesMessage(Message::IdentitySelected(a))
            }));
            list = list.push(row);
            i += 1;
        }

        let buttons = Row::new()
            .spacing(text_size / 4)
            .push(
                Button::new(
                    &mut self.copy_button,
                    Text::new("Copy to clipboard").size(text_size),
                )
                .on_press(gui::Message::IdentitiesMessage(Message::CopyPressed)),
            )
            .push(
                Button::new(
                    &mut self.subscribe_button,
                    Text::new("Subscribe").size(text_size),
                )
                .on_press(gui::Message::IdentitiesMessage(Message::SubscribePressed)),
            );

        let generate_random = Row::new().spacing(text_size / 4).push(
            Button::new(
                &mut self.generate_random_button,
                Text::new("Generate random").size(text_size),
            )
            .on_press(gui::Message::IdentitiesMessage(
                Message::GenerateRandomPressed,
            )),
        );

        let generate_deterministic = Row::new()
            .spacing(text_size / 4)
            .push(
                TextInput::new(
                    &mut self.deterministic_password,
                    "Password",
                    &self.deterministic_password_value,
                    |a| gui::Message::IdentitiesMessage(Message::DeterministicPasswordChanged(a)),
                )
                .size(text_size)
                .padding(text_size / 4),
            )
            .push(
                Button::new(
                    &mut self.generate_deterministic_button,
                    Text::new("Generate deterministic").size(text_size),
                )
                .on_press(gui::Message::IdentitiesMessage(
                    Message::GenerateDeterministicPressed,
                )),
            );

        let generate_chan = Row::new()
            .spacing(text_size / 4)
            .push(
                TextInput::new(
                    &mut self.chan_password,
                    "Password",
                    &self.chan_password_value,
                    |a| gui::Message::IdentitiesMessage(Message::ChanPasswordChanged(a)),
                )
                .size(text_size)
                .padding(text_size / 4),
            )
            .push(
                Button::new(
                    &mut self.generate_chan_button,
                    Text::new("Generate chan").size(text_size),
                )
                .on_press(gui::Message::IdentitiesMessage(
                    Message::GenerateChanPressed,
                )),
            );

        Column::new()
            .spacing(text_size / 4)
            .push(list)
            .push(buttons)
            .push(generate_random)
            .push(generate_deterministic)
            .push(generate_chan)
            .into()
    }
}
