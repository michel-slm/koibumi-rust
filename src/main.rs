//! __Koibumi__ is an experimental [Bitmessage](https://bitmessage.org/) client.
//! Note that Koibumi is __NOT__ an official project of The Bitmessage Developers.
//!
//! # Features
//!
//! Koibumi can connect to the Bitmessage network
//! and relay Bitmessage objects.
//!
//! It has a GUI.
//! Configuration and monitoring can be performed on the window.
//! If you do not need any GUI, use
//! [`koibumi-daemon`](https://crates.io/crates/koibumi-daemon) instead.
//!
//! Currently, this client can send and receive chan and broadcast messages.
//!
//! By default, network connections are limited to __via Tor only__.
//! In this case, you need a Tor SOCKS5 proxy running at localhost.
//!
//! The objects loaded from the network and
//! the list of known node addresses are saved on local file system by using SQLite.
//! Configurations can be saved to the file in the user's configuration directory.
//! The data directory can be changed by specifying a `-d` option on the command line.
//!
//! # Usage
//!
//! To install the Koibumi Bitmessage client, issue the command:
//!
//! ```sh
//! cargo install koibumi
//! ```
//!
//! Or, if you want to display Japanese characters:
//!
//! ```sh
//! cargo install koibumi --features unifont_jp
//! ```
//!
//! To complete the installation successful,
//! you may need to install some extra libraries
//! such as `libvulkan-dev` on the host Linux machine.
//!
//! To run the client, run `koibumi` command, and a GUI window popups.
//! Possibly, you may need to install `mesa-vulkan-drivers` on Linux.
//! On the window, you can configure several settings.
//!
//! To connect to the Bitmessage network, push __Start__ button on the window.
//! When the client connected to some remote nodes,
//! their addresses and user agents are shown on the window.
//! You can monitor how many Bitmessage objects are downloaded and shared.
//!
//! This client is experimental and under development,
//! so many debug logs are printed on the console.
//! Adding `-v` option on the command line, more messages are printed.
//!
//! Note that since database format can be changed among versions,
//! you may have to remove database files located at `$HOME/.config/koibumi` when trying new version.
//!
//! # Settings
//!
//! These are default settings:
//!
//! * Spawn a Bitmessage server at 127.0.0.1:8444.
//! But, currently, this does not fully work,
//! and the client does not advertise its server address.
//! * Connect to the Bitmessage network via SOCKS5 proxy at 127.0.0.1:9050.
//! It is the default local Tor proxy server address.
//! Be careful, if this checkbox is turned off, no SOCKS proxy is used,
//! and all connections are directly to Clearnet.
//! * The client can connect to Bitmessage nodes that have Onion addresses.
//! * The client can connect to Bitmessage nodes that have IP addresses.
//! Be careful, if SOCKS checkbox above is turned off,
//! then all connections are directly to Clearnet.
//! * Does not connect to myself.
//! * Use a user agent of a version of PyBitmessage.
//! * At first, connect to the seed Bitmessage node on the Tor network.
//! This default address is embedded in PyBitmessage source code.
//! * Max 160 incoming connections can be accepted
//! and max 128 incoming established connections can be managed.
//! * Max 32 outgoing connections can be initiated
//! and max 8 outgoing established connections are keeped.
//!
//! You can change these settings on the GUI window.
//!
//! # Implementation details
//!
//! Some of the significant external crates which Koibumi uses:
//!
//! * [`async-std`](https://crates.io/crates/async-std) for networking
//! * [`iced`](https://crates.io/crates/iced) for GUI
//!
//! Koibumi's internal implementation is divided into several crates:
//!
//! * Applications
//!   * [`koibumi`](https://crates.io/crates/koibumi):
//!     An experimental Bitmessage client with GUI
//!   * [`koibumi-daemon`](https://crates.io/crates/koibumi-daemon):
//!     An experimental Bitmessage client without GUI
//! * Libraries
//!   * [`koibumi-core`](https://crates.io/crates/koibumi-core):
//!     A library that provides various types and methods
//!     usable to implement Bitmessage clients
//!   * [`koibumi-node`](https://crates.io/crates/koibumi-node):
//!     An implementation of Bitmessage network node
//! * Other small libraries
//!   * [`koibumi-base32`](https://crates.io/crates/koibumi-base32):
//!     Minimal Base32 encoder/decoder for Onion addresses
//!   * [`koibumi-socks`](https://crates.io/crates/koibumi-socks):
//!     Minimal SOCKS5 client for Tor proxy

#![deny(unsafe_code)]
#![allow(dead_code)]

mod bridge;
mod config;
mod contacts;
/// Provides a GUI implementation by using
/// [`iced`](https://crates.io/crates/iced).
mod gui;
mod identities;
mod log;
mod messages;
mod send;
mod settings;
mod status;
mod style;
mod subscriptions;
mod tab;

use iced::{Application, Settings};

use gui::Gui;

/// The main entry point of the application.
#[cfg(not(feature = "unifont_jp"))]
fn main() {
    if let Err(err) = Gui::run(Settings::default()) {
        eprintln!("{}", err);
    }
}

/// The main entry point of the application.
#[cfg(feature = "unifont_jp")]
fn main() {
    if let Err(err) = Gui::run(Settings {
        default_font: Some(koibumi_unifont_jp::TTF),
        ..Settings::default()
    }) {
        eprintln!("{}", err);
    }
}
